const path = require('path')
const { Scenario } = require(path.resolve('sequelize', 'models'))
const db = require(path.resolve('sequelize', 'models', 'index.js'))

exports.getScenarios = (req, res, next) => {
    Scenario.findAll()
        .then(nodes => {
            res.status(200).json(nodes)
        })
        .catch(error => res.status(400).json({ error }))
}

// To get the nodes of a specific session
exports.getScenarioInfos = (req, res, next) => {
    return db.Scenario.findOne({
        where: { id: req.params.id }
    })
        .then(scenario => {
            res.status(200).json(scenario)
        });
}