const express = require('express');
const scenarioCtrl = require('../controllers/scenario');

const router = express.Router();

router.get('/', scenarioCtrl.getScenarios);
router.get('/get/:id', scenarioCtrl.getScenarioInfos);

module.exports = router;