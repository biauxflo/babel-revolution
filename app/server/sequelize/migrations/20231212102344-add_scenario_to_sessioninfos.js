'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn('SessionInfos', 'scenario', {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: ''
    })
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.removeColumn('SessionInfos', 'scenario');
  }
};
