'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn('Ends', 'scenario', {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: ''
    })
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.removeColumn('Ends', 'scenario');
  }
};
