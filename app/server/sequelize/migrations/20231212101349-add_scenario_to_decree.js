'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn('Decrees', 'scenarios=', {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: ''
    })
    await queryInterface.addColumn('Decrees', 'origin', {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: ''
    });
    await queryInterface.addColumn('Decrees', 'author', {
      type: Sequelize.STRING,
      allowNull: false,
      defaultValue: ''
    });
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.removeColumn('Decrees', 'scenario');
    await queryInterface.removeColumn('Decrees', 'origin');
  }
};
