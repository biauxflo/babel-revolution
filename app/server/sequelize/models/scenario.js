'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Scenario extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Scenario.init({
    author: DataTypes.STRING,
    title: DataTypes.STRING,
    language: DataTypes.STRING(3),
    prologue: DataTypes.TEXT({ length: 'long' })
  }, {
    sequelize,
    modelName: 'Scenario',
  });
  return Scenario;
};