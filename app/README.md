# babel_revolution
Dépôt du code source du projet BⱯBEL Rëvoluθion, projet de recherche-création collaboratif et contributif

## Installation

### Développement
En local, installer node.js et npm, puis lancer la commande `npm install` dans le dossier du projet.
Ensuite, installer nodemon avec la commande `npm install -g nodemon` pour pouvoir lancer le serveur avec la commande `nodemon app.js`.
Cela vous permettra de developper et de voir les modifications en live de la partie server (dossier `server`). Le site sera accessible à l'adresse `localhost:3000`.

Pour la partie client (dossier `client`), il suffit d'ouvrir le fichier `index.html` dans un navigateur.

### Pre-production

A l'aide de docker, lancer la commande `docker-compose up` dans le dossier du projet.
Cela vous permettra de mettre en place un environnement de pre-prod dans un composant docker et de tester les modifications effectuées avant d'être mis en prod sur les serveurs de l'UTC

### Production

Une fois que tout fonctionne correctement sous docker, contactez la DSI de l'UTC pour mettre en place les modifications sur le serveur de production `babel.utc.fr`.

## Organisation du projet

### Technologies utilisées

Le projet est divisé en deux parties : le client et le serveur.
Le serveur est codé en js et utilise le framework node.js et express.js pour gerer les differents appels backend.
Le client est codé en javascript et utilise la librairie d3.js pour gerer l'affichage d'un graphe dynamique.

La partie serveur utilise aussi le framework socket.io pour gerer les appels en temps réel entre le client et le serveur.
Enfin, le serveur utilise aussi sequelize pour gerer la connexion à la base de données sous forme d'ORM.

Toutes les documentations sont disponibles sur internet et les versions sont disponibles dans le fichier `package.json` pour les librairies et dans le fichier `Dockerfile` pour les composants docker.