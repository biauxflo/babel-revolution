"use strict";

/*********************************** IMPORTS ***************************************/
import { Message, SessionGlass, Prompt, GlassBase, PromptWithCancel } from "./adminElements.js";
import { addSessionHTML, createSession } from "./sessionCreation.js";

/*********************************** VARIABLES ***************************************/
const appMessage = new Message('#application_message');
const menu = new GlassBase('aside');

const sessionTemplate = document.querySelector("div#session_template");

const ongoingSessionsSet = document.querySelector("#ongoing_sessions_set");
const createSessionDiv = ongoingSessionsSet.querySelector("#create_session");
const visibleSessionsSet = document.querySelector("#visible_completed_sessions_set");
const nonVisibleSessionsSet = document.querySelector("#non_visible_completed_sessions_set");

const sessionActionsGlass = new SessionGlass('#session_actions_glass');

const sessionNamePrompt = new Prompt('#session_name_prompt');
const sessionRenamePrompt = new Prompt('#session_rename_prompt');
const sessionDeletePrompt = new PromptWithCancel('#session_delete_prompt');
const accountCreatePrompt = new PromptWithCancel('#account_create_prompt');
const accountDeletePrompt = new PromptWithCancel('#account_delete_prompt');
const passwordChangePrompt = new PromptWithCancel('#password_change_prompt');
const scenarioCreatePrompt = new PromptWithCancel('#scenario_create_prompt');

/*********************************** AT PAGE LOAD ***************************************/
// Get the sessions and update the page depending on the admin level of the current user
document.addEventListener("DOMContentLoaded", function () {
    // Get all the sessions that the current user is allowed to see and display them
    fetch('/admin/get-sessions', {
        method: 'get',
    })
        .then(response => response.json())
        .then(res => {
            if (res.success) {
                // Create the HTML element for each session
                res.sessions.forEach(session => {
                    fetch('/scenario/get/'+session.scenario, {
                        method: 'get',
                    }).then(responseScenario => responseScenario.json())
                    .then(resScenario => {
                        addSessionHTML(sessionTemplate, session, sessionActionsGlass, ongoingSessionsSet, createSessionDiv,
                            visibleSessionsSet, nonVisibleSessionsSet, sessionRenamePrompt, sessionDeletePrompt, appMessage, resScenario.title);
                    })
                });
            } else {
                throw new Error(res.error);
            }
        })
        .catch(error => {
            console.error('Error getting sessions : ', error);
            appMessage.showError('Erreur lors de la récupération des sessions.', 8000);
        });

    // Get the current user admin level, and update the html if needed
    fetch('/admin/admin-level', {
        method: 'get',
    })
        .then(response => response.json())
        .then(res => {
            if (res.success) {
                switch (res.privileges) {
                    case 0:
                        break;

                    case 1:
                        accountCreatePrompt.element.querySelector("#privilege_1_radio").remove();
                        accountCreatePrompt.element.querySelector("#privilege_1_radio_label").remove();
                        break;

                    default:
                        // For privileges === '2' (shall not be something else)
                        menu.element.querySelector("button#create_account").remove();
                        menu.element.querySelector("button#delete_account").remove();
                        break;
                }
            } else {
                throw new Error(res.error);
            }
        })
        .catch(error => {
            // Same thing that for admin level 2 by default
            menu.element.querySelector("button#create_account").remove();
            menu.element.querySelector("button#delete_account").remove();
            // And messages
            console.error('Error getting current admin level : ', error);
            appMessage.showError(
                'Erreur lors de la récupération du niveau admin, certaines fonctionnalités peuvent être indisponibles', 15000);
        });
});

/*********************************** PROMPTS ***************************************/
// Prompt to enter the name of a new session
sessionNamePrompt.prompt.querySelector("button.create").addEventListener('click', function () {
    const name = sessionNamePrompt.prompt.querySelector("input[name=name]").value;
    const isPublic = sessionNamePrompt.prompt.querySelector("select#privacy-list").value === "public";
    const scenario = sessionNamePrompt.prompt.querySelector("select#scenarios-list");
    createSession(sessionTemplate, name, isPublic, {id: scenario.value, title:  scenario.options[scenario.selectedIndex].label}, sessionActionsGlass, ongoingSessionsSet, createSessionDiv,
        visibleSessionsSet, nonVisibleSessionsSet, sessionRenamePrompt, sessionDeletePrompt, appMessage);
    sessionNamePrompt.hide();
});

// Prompt to change the name of a new session
sessionRenamePrompt.prompt.querySelector("button.rename").addEventListener('click', function () {
    const newTitle = sessionRenamePrompt.prompt.querySelector("input").value;
    const id = sessionRenamePrompt.associated.id;
    const data = new URLSearchParams({ id, newTitle });
    fetch('/admin/rename-session', {
        method: 'post',
        body: data
    })
        .then(response => response.json())
        .then(res => {
            if (res.success) {
                sessionRenamePrompt.associated.querySelector('.session_title').textContent = newTitle;
            } else {
                throw new Error(res.error);
            }
        })
        .catch(error => {
            appMessage.showError("Erreur lors du changement de nom de la session.");
            console.error('Error renaming session : ', error);
        });
    sessionRenamePrompt.hide();
});

// Prompt to confirm or cancel the deletion of a session
sessionDeletePrompt.prompt.querySelector("button.delete").addEventListener('click', function () {
    const id = sessionDeletePrompt.associated.id;
    const data = new URLSearchParams({ id });
    fetch('/admin/delete-session', {
        method: 'post',
        body: data
    })
        .then(response => response.json())
        .then(res => {
            if (res.success) {
                sessionDeletePrompt.associated.remove();
            } else {
                throw new Error(res.error);
            }
        })
        .catch(error => {
            appMessage.showError("Erreur lors de la suppression de la session.");
            console.error('Error deleting session : ', error);
        });
    sessionDeletePrompt.hide();
});

/*********************************** SESSION CREATION ***************************************/
// When the user click on the '+' session, it shows the prompt to enter the name of the new session
createSessionDiv.addEventListener('click', function () {
    sessionNamePrompt.prompt.querySelector("select#privacy-list").value = "public";

    fetch('/admin/all-scenario-list')
            .then(response => response.json())
            .then(res => {
                if (res.success) {
                    res.scenarios.forEach(scenario => {
                        const option = document.createElement('option');
                        option.value = scenario.id;
                        option.label = scenario.title;
                        sessionNamePrompt.element.querySelector('select#scenarios-list').appendChild(option);
                    });
                } else {
                    throw new Error(res.error);
                }
            })
            .catch(error => {
                console.error('Error fetching scenarios : ', error);
            });
    sessionNamePrompt.show();
    // On this prompt, when the user will click on "Créer", it will call the function createSession() 
});

sessionNamePrompt.prompt.querySelector("select#privacy-list").addEventListener('change', function () {
    let text;
    if (sessionNamePrompt.prompt.querySelector("select#privacy-list").value === "public")
        text = "Publique : la session sera visible sur l'accueil de BABEL REVOLUTION et accessible à tous.";
    else
        text = "Privée : la session ne sera pas visible sur l'accueil de BABEL REVOLUTION et accessible uniquement aux possesseurs du lien.";
    sessionNamePrompt.prompt.querySelector("#privacy-info").textContent = text;
});

/*********************************** MENU ***************************************/
// When the user clicks on the 'Menu' button, the menu appears
document.querySelector('#menu_button').addEventListener('click', function () {
    menu.show();
});

// When the user clicks on another place than on the menu, the menu disappeared
document.querySelector('#menu_hiding_glass').addEventListener('click', function () {
    menu.hide();
});

/*********************************** CREATE ACCOUNT ***************************************/
// When the user select the privileges level, it shows the correspondant accesses
const privileges1Radio = accountCreatePrompt.element.querySelector('input#privilege_1_radio');
const privileges2Radio = accountCreatePrompt.element.querySelector('input#privilege_2_radio');
const privileges1List = accountCreatePrompt.element.querySelector('#privileges_1');
const privileges2List = accountCreatePrompt.element.querySelector('#privileges_2');
function updatePrivilegeText() {
    if (privileges1Radio.checked) {
        privileges1List.style.zIndex = "30";
        privileges2List.style.zIndex = "-30";
    } else if (privileges2Radio.checked) {
        privileges1List.style.zIndex = "-30";
        privileges2List.style.zIndex = "30";
    } else {
        privileges1List.style.zIndex = "-30";
        privileges2List.style.zIndex = "-30";
    }
}
privileges1Radio.addEventListener('click', updatePrivilegeText);
privileges2Radio.addEventListener('click', updatePrivilegeText);

const createAccountMessage = accountCreatePrompt.element.querySelector("#create_account_message");

// When the user clicks on the button "Créer un compte", it shows the prompt
menu.element.querySelector('button#create_account').addEventListener('click', function () {
    menu.hide();
    updatePrivilegeText();
    // We reset the error message
    createAccountMessage.textContent = "";
    accountCreatePrompt.show();
});

// This function is called when the user clicks on the submit button of the account create prompt
function sendAccountCreationData(formInfo) {
    // We reset the message, then we send the data
    createAccountMessage.textContent = "";
    const data = new URLSearchParams(formInfo);
    fetch('admin/create-account', {
        method: 'post',
        body: data
    })
        .then(response => response.json())
        .then(res => {
            if (res.success) {
                appMessage.showMessage("Le compte a été créé.");
                accountCreatePrompt.hide();
            } else {
                throw new Error(res.error);
            }
        })
        .catch(error => {
            createAccountMessage.textContent = "Erreur serveur lors de la création du compte.";
            console.error('Error creating the account : ', error);
        });
}

const createAccountForm = accountCreatePrompt.element.querySelector("#create_account_form");
createAccountForm.addEventListener('submit', function (event) {
    event.preventDefault();
    const formInfo = new FormData(createAccountForm);
    // We check the info from the form
    if (formInfo.get('username').length < 1) {
        createAccountMessage.textContent = "Le nom d'utilisateur ne peut pas être nul.";

    } else if (formInfo.get('password').length < 5) {
        createAccountMessage.textContent = "Le mot de passe doit avoir 5 caractères ou plus.";

    } else if (formInfo.get('password') !== formInfo.get('password_verification')) {
        createAccountMessage.textContent = "Les mots de passe doivent correspondre.";

    } else if (formInfo.get('privilege') !== '1' && formInfo.get('privilege') !== '2') {
        createAccountMessage.textContent = "Le niveau admin doit être 1 ou 2.";

    } else {
        // If everything until now is ok, we now check if the username is already taken
        fetch('admin/all-accounts-list')
            .then(response => response.json())
            .then(res => {
                if (res.success) {
                    // We check for the elements in res.users if the username is already included
                    if (res.users.some(element => element.username === formInfo.get('username'))) {
                        createAccountMessage.textContent = "Ce nom d'utilisateur est déjà utilisé.";

                    } else {
                        sendAccountCreationData(formInfo);
                    }
                } else {
                    throw new Error(res.error);
                }
            })
            .catch(error => {
                console.error('Error fetching accounts : ', error);
                // We still continue, if the username is already taken, the db will returns an error itself
                sendAccountCreationData(formInfo);
            });

    }
});

/*********************************** DELETE ACCOUNT ***************************************/
const deleteAccountMessage = accountDeletePrompt.element.querySelector("#delete_account_message");

// When the user clicks on the button "Supprimer un compte", it updates the list of accounts then it shows the prompt
menu.element.querySelector('button#delete_account').addEventListener('click', () => {
    deleteAccountMessage.textContent = "";
    const accountsSelect = accountDeletePrompt.element.querySelector('select.account');
    // Send a GET request to get the list of the accounts
    fetch('/admin/accounts-list')
        .then(response => response.json())
        .then(res => {
            if (res.success) {
                // Delete previous content 
                accountsSelect.innerHTML = '';
                // Add the accounts to the select object
                res.users.forEach(user => {
                    const option = document.createElement('option');
                    option.value = user.username;
                    option.label = user.username;
                    accountsSelect.appendChild(option);
                });
            } else {
                throw new Error(res.error);
            }
        })
        .catch(error => {
            deleteAccountMessage.textContent = "Les comptes n'ont pas été récupérés.";
            console.error('Error fetching accounts : ', error);
        });
    menu.hide();
    accountDeletePrompt.show();
});

// When the user clicks on the prompt button "Supprimer le compte", it hides the prompt
const deleteAccountForm = accountDeletePrompt.element.querySelector("#delete_account_form");
deleteAccountForm.addEventListener('submit', function (event) {
    event.preventDefault();
    const data = new URLSearchParams(new FormData(deleteAccountForm));
    fetch('admin/delete-account', {
        method: 'post',
        body: data
    })
        .then(response => response.json())
        .then(res => {
            if (res.success) {
                appMessage.showMessage("Le compte a été supprimé.");
                accountDeletePrompt.hide();
            } else {
                throw new Error(res.error);
            }
        })
        .catch(error => {
            deleteAccountMessage.textContent = "Le compte n'a pas été supprimé.";
            console.error('Error fetching accounts : ', error);
        });
});

/*********************************** CHANGE PASSWORD ***************************************/
const passwordChangeMessage = passwordChangePrompt.element.querySelector("#password_change_message");

// When the user clicks on the button "Créer un compte", it shows the prompt
menu.element.querySelector('button#change_password').addEventListener('click', function () {
    passwordChangeMessage.textContent = "";
    menu.hide();
    passwordChangePrompt.show();
});

// When the user clicks on the prompt button "Créer le compte", it hides the prompt
const passwordChangeForm = passwordChangePrompt.element.querySelector("#password_change_form");
passwordChangeForm.addEventListener('submit', function (event) {
    event.preventDefault();
    const formInfo = new FormData(passwordChangeForm);
    // We check the password (it will also be checked on the server)
    if (formInfo.get('password').length < 5) {
        passwordChangeMessage.textContent = "Le mot de passe doit avoir 5 caractères ou plus.";
        return;
    } else if (formInfo.get('password') !== formInfo.get('password_verification')) {
        passwordChangeMessage.textContent = "Les mots de passe doivent correspondre.";
        return;
    }
    // We send the new password
    const data = new URLSearchParams(formInfo);
    fetch('admin/change-password', {
        method: 'post',
        body: data
    })
        .then(response => response.json())
        .then(res => {
            if (res.success) {
                appMessage.showMessage("Le mot de passe a été changé.");
                passwordChangePrompt.hide();
            } else {
                throw new Error(res.error);
            }
        })
        .catch(error => {
            passwordChangeMessage.textContent = "Le mot de passe n'a pas été changé.";
            console.error('Error changing password : ', error);
        });
});


/*********************************** CREATE SCENARIO ***************************************/

// Everywhere
const nextButton = scenarioCreatePrompt.element.querySelectorAll('.next-step');
for (let button of nextButton) button.addEventListener('click', changeScenarioStep);
const closeButtons = document.querySelectorAll(".close");
closeButtons.forEach(closeButton => closeButton.addEventListener('click', () => resetScenarioForm()));

// Step 1 : General
const titleScenario = scenarioCreatePrompt.element.querySelector('#title');
const languageScenario = scenarioCreatePrompt.element.querySelector('#language');
const prologueScenario = scenarioCreatePrompt.element.querySelector('#prologue');

// Step 2 : Origin
const titleOrigin = scenarioCreatePrompt.element.querySelector('#title_origin');
const authorOrigin = scenarioCreatePrompt.element.querySelector('#author_origin');
const contentOrigin = scenarioCreatePrompt.element.querySelector('#content_origin');

// Step 3 : Decree
const titleDecree = scenarioCreatePrompt.element.querySelector('#title_decree');
const authorDecree = scenarioCreatePrompt.element.querySelector('#author_decree');
const contentDecree = scenarioCreatePrompt.element.querySelector('#content_decree');
const buttonCancelDecree = scenarioCreatePrompt.element.querySelector('button#cancel_decree');
const buttonAddDecree = scenarioCreatePrompt.element.querySelector('button#add_decree');
const buttonNewDecree = scenarioCreatePrompt.element.querySelector('button#new_decree');
const buttonsAddDecree = scenarioCreatePrompt.element.querySelector('.scenario_decree .buttons-add');
const buttonsNextDecree = scenarioCreatePrompt.element.querySelector('.scenario_decree .buttons-next');
buttonCancelDecree.addEventListener('click', cancelDecree);
buttonAddDecree.addEventListener('click', addDecreeInfo);
buttonNewDecree.addEventListener('click', nextDecree);

// Step 4 : End
const titleEnd = scenarioCreatePrompt.element.querySelector('#title_end');
const contentEnd = scenarioCreatePrompt.element.querySelector('#content_end');
const buttonCancelEnd = scenarioCreatePrompt.element.querySelector('button#cancel_end');
const buttonAddEnd = scenarioCreatePrompt.element.querySelector('button#add_end');
const buttonNewEnd = scenarioCreatePrompt.element.querySelector('button#new_end');
const buttonsAddEnd = scenarioCreatePrompt.element.querySelector('.scenario_end .buttons-add');
const buttonsNextEnd = scenarioCreatePrompt.element.querySelector('.scenario_end .buttons-next');
buttonCancelEnd.addEventListener('click', cancelEnd);
buttonAddEnd.addEventListener('click', addEndInfo);
buttonNewEnd.addEventListener('click', nextEnd);

const buttonAddScenario = scenarioCreatePrompt.element.querySelector('button#add_scenario');
buttonAddScenario.addEventListener('click', addScenario);

// Init content
titleScenario.value = "";
languageScenario.value = "FRA";
prologueScenario.value = "";
titleOrigin.value = "";
authorOrigin.value = "";
contentOrigin.value = "";
titleDecree.value = "";
authorDecree.value = "";
contentDecree.value = "";
titleEnd.value = "";
contentEnd.value = "";

let decrees;
let ends;

let scenarioStep = {
    type: "general",
    index: 0
}

function changeScenarioStep(e) {
    e.preventDefault();
    switch (scenarioStep.type) {
        case "general":
            if (titleScenario.value.trim() !== "" && languageScenario.value.trim() !== "" && prologueScenario.value.trim() !== "") {
                scenarioStep = {
                    type: "origin",
                    index: 0
                }
                scenarioCreatePrompt.element.querySelector('.scenario_general').classList.add('hidden');
                scenarioCreatePrompt.element.querySelector('.scenario_origin').classList.remove('hidden');
            }
            else
                appMessage.showMessage("Informations manquantes");
            break;
    case "origin":
        if (titleOrigin.value.trim() !== "" && authorOrigin.value.trim() !== "" && contentOrigin.value.trim() !== "") {
            scenarioStep = {
                type: "decree",
                index: 0
            }
            scenarioCreatePrompt.element.querySelector('.scenario_origin').classList.add('hidden');
            scenarioCreatePrompt.element.querySelector('.scenario_decree').classList.remove('hidden');
        }
        else
            appMessage.showMessage("Informations manquantes");
        break;
    case "decree":
        scenarioStep = {
            type: "end",
            index: 0
        }
        scenarioCreatePrompt.element.querySelector('.scenario_decree').classList.add('hidden');
        scenarioCreatePrompt.element.querySelector('.scenario_end').classList.remove('hidden');
        break;
    case "end":
        scenarioStep = {
            type: "general",
            index: 0
        }
        scenarioCreatePrompt.element.querySelector('.scenario_end').classList.add('hidden');
        scenarioCreatePrompt.element.querySelector('.scenario_general').classList.remove('hidden');
        break;
    }
}

// When the user clicks on the button "Créer un scenario", it shows the prompt
menu.element.querySelector('button#create_scenario').addEventListener('click', function () {
    decrees = [];
    ends = [];
    menu.hide();
    // We reset the error message
    //createScenarioMessage.textContent = "";
    scenarioCreatePrompt.show();
});

function cancelDecree() {
    titleDecree.value = "";
    authorDecree.value = "";
    contentDecree.value = "";
    titleDecree.disabled = true;
    authorDecree.disabled = true;
    contentDecree.disabled = true;
    buttonsNextDecree.classList.remove('hidden');
    buttonsAddDecree.classList.add('hidden');
}

function addDecreeInfo() {
    if (titleDecree.value.trim() != "" && contentDecree.value.trim() != "" && authorDecree.value.trim() != "") {
        let decree = {
            text: contentDecree.value,
            title: titleDecree.value,
            author: authorDecree.value,
        }
        decrees.push(decree);
        titleDecree.disabled = true;
        authorDecree.disabled = true;
        contentDecree.disabled = true;
        buttonCancelDecree.classList.remove('hidden');
        buttonsNextDecree.classList.remove('hidden');
        buttonsAddDecree.classList.add('hidden');
        appMessage.showMessage("Decret configuré");
        scenarioStep.index++;
    }
    else
        appMessage.showMessage("Informations manquantes");
}

function nextDecree() {
    titleDecree.value = "";
    authorDecree.value = "";
    contentDecree.value = "";
    titleDecree.disabled = false;
    authorDecree.disabled = false;
    contentDecree.disabled = false;
    buttonsNextDecree.classList.add('hidden');
    buttonsAddDecree.classList.remove('hidden');
    scenarioCreatePrompt.element.querySelector('.scenario_decree h3').textContent = "Décret - " + (scenarioStep.index + 1);
}

function cancelEnd() {
    titleEnd.value = "";
    contentEnd.value = "";
    titleEnd.disabled = true;
    contentEnd.disabled = true;
    buttonsNextEnd.classList.remove('hidden');
    buttonsAddEnd.classList.add('hidden');
}

function addEndInfo() {
    if (titleEnd.value.trim() != "" && contentEnd.value.trim() != "") {
        let end = {
            text: contentEnd.value,
            title: titleEnd.value,
        }
        ends.push(end);
        titleEnd.disabled = true;
        contentEnd.disabled = true;
        buttonCancelEnd.classList.remove('hidden');
        buttonsNextEnd.classList.remove('hidden');
        buttonsAddEnd.classList.add('hidden');
        appMessage.showMessage("Fin configurée");
        scenarioStep.index++;
    }
    else
        appMessage.showMessage("Informations manquantes");
}

function nextEnd() {
    titleEnd.value = "";
    contentEnd.value = "";
    titleEnd.disabled = false;
    contentEnd.disabled = false;
    buttonsNextEnd.classList.add('hidden');
    buttonsAddEnd.classList.remove('hidden');
    scenarioCreatePrompt.element.querySelector('.scenario_end h3').textContent = "Fin - " + (scenarioStep.index + 1);
}

function addScenario(e) {
    e.preventDefault();
    const data = {
        title: titleScenario.value,
        language: languageScenario.value,
        prologue: prologueScenario.value
    };
    
    fetch('admin/create-scenario', {
        method: 'post',
        body: new URLSearchParams(data)
    })
        .then(response => response.json())
        .then(res => {
            if (res.success) {
                titleScenario.value = "";
                languageScenario.value = "FRA";
                prologueScenario.value = "";
                addDecrees(res.id);
                addEnds(res.id);
                appMessage.showMessage("Le scénario a été créé.");
                scenarioCreatePrompt.hide();
                resetScenarioForm();
            } else {
                resetScenarioForm();
                throw new Error(res.error);
            }
        })
        .catch(error => {
            appMessage.showMessage("Erreur serveur lors de la création du scénario");
            console.error('Error creating the scenario : ', error);
            resetScenarioForm();
            });
}

function addDecrees(scenarioId) {
    const origin = {
        title: titleOrigin.value,
        text: contentOrigin.value,
        author: authorOrigin.value,
        scenario: scenarioId,
        origin: true
    }
    fetch('admin/create-decree', {
        method: 'post',
        body: new URLSearchParams(origin)
    })
        .then(response => response.json())
        .then(res => {
            if (res.success) {
                // Reset origin
            } else {
                throw new Error(res.error);
            }
        })
        .catch(error => {
            appMessage.showMessage("Erreur serveur lors de la création de l'origine");
            console.error('Error creating the origin : ', error);
        });
    decrees.forEach((decree, index) => {
        const data = {
            title: decree.title,
            text: decree.text,
            author: decree.author,
            scenario: scenarioId,
            origin: false
        }
        fetch('admin/create-decree', {
            method: 'post',
            body: new URLSearchParams(data)
        })
            .then(response => response.json())
            .then(res => {
                if (res.success) {
                    // Reset decree
                } else {
                    throw new Error(res.error);
                }
            })
            .catch(error => {
                appMessage.showMessage("Erreur serveur lors de la création des décrets");
                console.error('Error creating the decree : ', error);
            });
    })
}

function addEnds(scenarioId) {
    ends.forEach(end => {
        const data = {
            title: end.title,
            text: end.text,
            scenario: scenarioId
        }
        fetch('admin/create-end', {
            method: 'post',
            body: new URLSearchParams(data)
        })
            .then(response => response.json())
            .then(res => {
                if (res.success) {
                    //appMessage.showMessage("La fin a été créée.");
                } else {
                    throw new Error(res.error);
                }
            })
            .catch(error => {
                appMessage.showMessage("Erreur serveur lors de la création des fins du scénario");
                console.error('Error creating the end : ', error);
            });
    })
}

function resetScenarioForm() {
    scenarioStep = {
        type: "general",
        index: 0
    }
    titleScenario.value = "";
    languageScenario.value = "FRA";
    prologueScenario.value = "";
    titleOrigin.value = "";
    authorOrigin.value = "";
    contentOrigin.value = "";
    titleDecree.value = "";
    authorDecree.value = "";
    contentDecree.value = "";
    titleEnd.value = "";
    contentEnd.value = "";
    titleDecree.disabled = false;
    authorDecree.disabled = false;
    contentDecree.disabled = false;
    titleEnd.disabled = false;
    contentEnd.disabled = false;
    decrees = [];
    ends = [];
    scenarioCreatePrompt.element.querySelector('.scenario_general').classList.remove('hidden');
    scenarioCreatePrompt.element.querySelector('.scenario_origin').classList.add('hidden');
    scenarioCreatePrompt.element.querySelector('.scenario_decree').classList.add('hidden');
    scenarioCreatePrompt.element.querySelector('.scenario_end').classList.add('hidden');
    scenarioCreatePrompt.element.querySelector('.scenario_decree h3').textContent = "Décret - 1";
    scenarioCreatePrompt.element.querySelector('.scenario_end h3').textContent = "Fin - 1";
    buttonsAddDecree.classList.remove('hidden');
    buttonsNextDecree.classList.add('hidden');
    buttonsAddEnd.classList.remove('hidden');
    buttonsNextEnd.classList.add('hidden');
    buttonCancelDecree.classList.add('hidden');
    buttonCancelEnd.classList.add('hidden');
}