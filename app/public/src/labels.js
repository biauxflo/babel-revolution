import {getNodeColor, getNodeDatas} from "./nodes.js";

const nodeTextDiv = d3.select("#node-text");
const nodeTitle = d3.select("#node-title");
const nodeAuthor = d3.select("#node-author");
const writtenBy = d3.select("#written-by"); //div to show before author

function onLabelClick(event, d, fetchedNodes, nodeSelection, selectReact) {
    let datas = getNodeDatas(d, fetchedNodes);
    let id = datas.id
    nodeTitle.html(datas.title);
    writtenBy.style("display", "block")
    nodeAuthor.html(datas.author);
    nodeTextDiv.html(datas.text);
    
    nodeSelection.style("fill", function(d){
        let currentColor = d3.select(this).style("fill");
        let color
        if (currentColor == undefined || currentColor == "rgb(28, 2, 126)") { // = #1c027e
            let datas = getNodeDatas(d, fetchedNodes);
            color = getNodeColor(datas, d.depth);
        }
        else
            color = currentColor
        return color;
    }) //reset color on all nodes 
    nodeSelection.filter(d => String(id) === d.data.data.id).style("fill", "#1c027e");
    d.data.color = "#1c027e";
    nodeTitle.style("display", "block") //show title, otherwise hidden

    //open side bar
    d3.select("#add-node-form").style("display", "none")
    d3.select("#messages-inside").style("display", "block")
    d3.select("#span-toggle-writing").html("⇩")
    d3.select("#span-toggle-messages").html("⇧")
    
    if (selectReact)
        selectReact.value = datas.id;
}

// Create label when using d3-hierarchy
// svg : svg element
// node : svg selection of nodes
// nodes : hierarchy datas for the graph
// fetchedNodes : nodes as stored in DB
export function createLabels(svg, nodeSelection, nodes, fetchedNodes, selectReact) {

    let label = svg
        .append('g')
        .selectAll('text')
        .data(nodes)
        .join('text')
        .attr('class', 'label')
        .style('fill', 'white')
        .style('stroke', 'none')
        .style('opacity', 1)
        .style("font-size", "14px")
        .attr('text-anchor', 'middle')
        .attr('id', d => d.data.data.id)
        .text(function (d) {
            let datas = getNodeDatas(d, fetchedNodes);
            let label = datas.title.length <= 9 ? datas.title : datas.title.slice(0, 9) + '…';
            return label
        })
        // Display node infos
        .on("click", function (event,d){
            onLabelClick(event, d, fetchedNodes, nodeSelection, selectReact)
        })
    return label;
}

export function createBackground(svg, nodeSelection, nodes, fetchedNodes, selectReact) {

    let label = svg
        .append('g')
        .selectAll('text')
        .data(nodes)
        .join('rect')
        .style("fill", "black")
        .attr("width", function (d) {
            let datas = getNodeDatas(d, fetchedNodes);
            let length = datas.title.length <= 9 ? datas.title.length : 10;
            let width = length * 13
            width = width < 30 ? 30 : width
            return width
        })
        .attr("height", 30)
        .attr("rx", 10)
        .attr("ry", 10)
        .attr("stroke", "white")
        .attr('x', function (d) {
            return d.x
          })
          .attr('y', function (d) {
            return d.y
          })
        // Display node infos
        .on("click", function (event,d){
            onLabelClick(event, d, fetchedNodes, nodeSelection, selectReact)
        })
    return label;
}

// N'est plus utilisé mais pourrait être nécessaire pour optimisé lors de l'actualisation du graph si nouveau noeud
export function joinLabels(svg, nodeSelection, nodes, fetchedNodes, selectReact) {
    let label = svg
        .append('g')
        .selectAll('text')
        .data(nodes)
        .join('text')
        .attr('class', 'label')
        .style('fill', 'white')
        .style('stroke', 'none')
        .style('opacity', 1)
        .style("font-size", "14px")
        .attr('text-anchor', 'middle')
        .attr('id', d => d.data.data.id)
        .text(function (d) {
            let datas = getNodeDatas(d, fetchedNodes);
            let label = datas.title.length <= 9 ? datas.title : datas.title.slice(0, 9) + '…';
            return label
        })
        // Display node infos
        .on("click", function (event,d){
            onLabelClick(event, d, fetchedNodes, nodeSelection, selectReact)
        })

    return label
}