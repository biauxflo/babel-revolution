USE babel_revolution_prod;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `babel_revolution_prod`
--

-- --------------------------------------------------------

--
-- Structure de la table `scenario`
--

CREATE TABLE IF NOT EXISTS `Scenarios` (
  `id` int NOT NULL AUTO_INCREMENT,
  `author` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `language` varchar(3) NOT NULL,
  `prologue` longtext,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci, AUTO_INCREMENT=1;

INSERT INTO `Scenarios` (`id`, `author`, `title`, `language`, `prologue`) VALUES
(1, 'BABEL TEAM', 'Centre de la monoculture', 'FRA', '« R-Pollëynisez la vi(ll)e ! » Malgré la censure, ce mystérieux tag fleurit sur les murs de la ville comme sur ceux des réseaux sociaux, à chaque nouveau décret du Centre de la Monoculture, le CMC, toute puissante institution agro-culturelle régissant à la fois l’agroforesterie et la vie intellectuelle. À quoi exhorte-t-il exactement ? Nul ne le sait précisément. Certains affirment que ce sont simplement des poètes qui veulent replanter des roses dans le bitume ; d’autres que ce sont des nostalgiques de la polyculture archaïque ; d’autres enfin que ce sont des insurgés fomentant un coup d’État… Le tag se multiplie à l’annonce prochaine d’un nouveau décret qui supprimerait des centaines de noms de fleurs, de plantes, d’arbres du Dictionnaire officiel du CMC, dernier d’une longue série de lois et amendements visant à expurger la Langue commune pour « l’actualiser et la mettre mieux en adéquation avec l’environnement », comme le stipule le Dictionnaire officiel du CMC. De fait, la biodiversité végétale s’est drastiquement réduite et les mots pour le dire avec elle.');

--
-- Structure de la table `Decrees`
--

CREATE TABLE IF NOT EXISTS `Decrees` (
  `id` int NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `text` longtext,
  `scenario` int NOT NULL,
  `origin` tinyint NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`scenario`) REFERENCES Scenarios(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `Decrees`
--

INSERT INTO `Decrees` (`id`, `title`, `author`, `text`, `scenario`, `origin`, `createdAt`, `updatedAt`) VALUES
(1, 'Décret sur les plantes', 'CMC', 'Il est désormais interdit d\'utiliser des hyponymes de « fleurs », d’« arbre » ou de « légume », comme pâquerette, mimosa, magnolia, artichaut, potiron, pomme de terre ou patate... Les termes scientifiques pour décrire une fleur, une plante ou un arbre sont bannis en raison de leur inutilité suite à la disparition de ces végétaux et des risques de mécompréhension. Devront être utilisées des expressions génériques et descriptives comme « fleur rose », « grand/petit arbre », « légume vert » ou « petit légume rond ».\r\nCes usages linguistiques sont évidemment corrélés à une culture agroalimentaire privilégiant la monoculture des fruits et légumes résistants et offrant de bons rendements pour nourrir le plus grand nombre.\r\nTout usage linguistique et cultural non conventionnel sera sanctionné. -- Le CMC\r\n', 1, 0, '2023-05-05 21:00:30', '2023-05-05 21:00:30'),
(2, 'Décret sur les langues', 'CMC', 'Suite à la prolifération d’usages linguistiques non conventionnels, le CMC rappelle que la Langue commune est la seule langue de la République autorisée.\r\nLa simplicité et l’intercompréhension étant les objectifs principaux du CMC, l’emploi dans la sphère publique, professionnelle et privée d’une langue autre que la langue commune est désormais formellement interdit. Cette interdiction s\'étend à tous les patois. Les termes inventés ou les usages de la langue non conforme au règlement syntaxique et orthographique, comme le verlan ou l’écriture phonétique sont aussi fermement prohibés. Ces pratiques excluent en effet des communications les personnes qui ne peuvent les comprendre.\r\nPour le bien commun, utilisons une seule langue expurgée de mots d’origine étrangère, gage de paix, d’unité et d\'intercompréhension, tout comme nous soutenons la monoculture qui simplifie la pratique culturale et augmente la production pour le bénéfice commun.\r\nTout usage linguistique et cultural non conventionnel sera sanctionné. -- Le CMC\r\n\r\n', 1, 0, '2023-05-05 21:00:30', '2023-05-05 21:00:30'),
(3, 'Décret sur la littérature', 'CMC', 'Des études scientifiques ont démontré le nombre élevé de termes polysémiques, susceptibles de qualifier deux réalités diverses : ces mots à double sens nuisent à la clarté de la compréhension et à l’efficacité de la communication. Les jeux de mots humoristiques sont proscrits. De même, les textes littéraires deviennent illégaux, en raison du recours à des qualificatifs désuets (caustique, acrimonieux, affable, prévenant…) autres que ceux de la liste des adjectifs recommandés (liste consultable sur le site du CMC), tels que gentil, méchant, bon, mauvais.\r\nPlus encore, toute métaphore florale ou végétale est proscrite en raison de leur caractère incitatif à la polyculture, dangereuse pour le bien commun.\r\nTout usage linguistique et cultural non conventionnel sera sanctionné. -- Le CMC\r\n', 1, 0, '2023-05-05 21:00:30', '2023-05-05 21:00:30'),
(4, 'Decree on Plants', 'CMC', 'It is now forbidden to use specific names for \"flower\", \"tree\" or \"vegetable\", such as daisy, mimosa, magnolia, artichoke, pumpkin, potato... Scientific terms used to describe a flower, plant or tree are banned, as they are no longer useful because of the disappearance of these plants and the risk of misunderstanding. Instead, generic, descriptive expressions such as \"pink flower\", \"large/small tree\", \"green vegetable\" or \"small round vegetable\" will be used.\r\nThese linguistic usages are obviously correlated with an agri-food culture that favours the monoculture of hardy fruit and vegetables offering good yields to feed the greatest number of people.\r\nAny unconventional use of language or culture will be punished. -- The CMC\r\n', 1, 0, '2023-05-05 21:00:30', '2023-05-05 21:00:30'),
(5, 'Decree on Languages', 'CMC', 'Following the proliferation of unconventional linguistic usages, the CMC wishes to point out that the Common Language is the only authorised language of the Republic.\r\nAs simplicity and mutual understanding are the CMC\'s main objectives, the use of a language other than the Common Language in the public, professional and private spheres is now formally prohibited. This prohibition extends to all dialects. Invented terms or uses of language that do not comply with syntax and spelling rules, such as slang or phonetic writing, are also firmly prohibited. These practices exclude from communication people who cannot understand them.\r\nFor the common good, let\'s use a single language purged of words of foreign origin, a guarantee of peace, unity and mutual understanding, just as we support monoculture, which simplifies farming practices and increases production for the common good.\r\nAny unconventional use of language or culture will be punished. -- The CMC\r\n', 1, 0, '2023-05-05 21:00:30', '2023-05-05 21:00:30'),
(6, 'Decree on Literature', 'CMC', 'Scientific studies have shown that there are a large number of polysemous terms, likely to describe two different realities: these double-meaning words are detrimental to clear understanding and effective communication. Humorous wordplay is outlawed. Similarly, literary texts are illegal if they use outdated adjectives (caustic, acrimonious, affable, considerate, etc.) other than those on the list of recommended adjectives (which can be consulted on the CMC website), such as nice, nasty, good, bad.\r\nWhat\'s more, all floral or plant metaphors are banned because they encourage polyculture, which is dangerous for the common good.\r\nAny unconventional linguistic or cultural usage will be punished. -- The CMC\r\n', 1, 0, '2023-05-05 21:00:30', '2023-05-05 21:00:30'),
(7, 'Centre de la monoculture', 'CMC', 'Au service du Bien commun et de l’intercompréhension entre les peuples, le Centre de la Monoculture œuvre à la mise à jour et à l’actualisation de la Langue commune pour la mettre toujours mieux en adéquation avec l’environnement culturel et cultural. Tout usage linguistique et cultural non conventionnel sera sanctionné. Le CMC', 1, 1, '2023-05-05 21:00:30', '2023-05-05 21:00:30');

-- --------------------------------------------------------

--
-- Structure de la table `Ends`
--

CREATE TABLE IF NOT EXISTS `Ends` (
  `id` int NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` longtext,
  `scenario` int NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`scenario`) REFERENCES Scenarios(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `Ends`
--

INSERT INTO `Ends` (`id`, `title`, `text`, `scenario`, `createdAt`, `updatedAt`) VALUES
(1, 'Fin - Victoire du soulèvement populaire', 'R como la Vittoria | comme-un·e !\r\n \r\nCelle que, full foule en houle, iouave portée du clavier ô bitume, apicamaramages.  \r\n\r\nDies irae : affamés d’oca, de rutabagas et de justicia, de radis et de diversity, nous avons quitté la Kverne de la Darkmachine.\r\n\r\nComme-Un·e, nous avons réussi à remonter la piste perdue de l’Apokalypsens bank, jusqu’au Global Seed Vault, dans l’Archipel de Svaltad, là où étaient conservées toutes les semences du monde agriKôl, où étaient conservés 15 000 années de semences.\r\n\r\nQue tesouro inestimável : des décennies de trésors agricoles amassés, perdus et oubliés sous le permafrost après la Troisième Guerre Mondiale. Seuls, nous n’avions pas les mots pour en trouver la trace.  Ensemble et polyphones, nous avons été fuerza viva : nous avons fait la révolution de rêves en graines.  Grâce à vous, déjà, des semailles semées à tous les vents lors de notre expédition victorieuse retour feront repousser toutes les plantes du monde : pois chiche d’Irak, patates du Pérou, blé du Berry... Sur les ruines du CMC, dévasté, poussent déjà au milieu des herbes folles une corne d’abondance et d’espera. L’onde irrigue désormais en rivière d’eau dulce et de mots disentravés, nos terres débridées.\r\n\r\nLe temps fera refleurir et nourrira tous les appétits, sans épuiser les terres pour emplir les bourses des monopuissances.\r\n\r\n Grâce aux savoirs ancestraux lovés au cœur des langues revivifiées, nous cultiverons à nouveau les plantes médicinales. Les malades guériront.\r\n\r\nLibres d’être vous-mêmes en vos langues, apaisez vos colères : déjà certains troquent les armes pour les bêches et les râteaux. Un jardin de sol y d’air sort déjà de terre, à l’orée de l’été.\r\n\r\nÉteins ta machine et deuz, viene, come, en toutes tes langues, pour cultiver et babiller notre jardin Comme-Un·e : que Babel rêve !', 1, '2023-05-05 21:03:47', '2023-05-05 21:03:47'),
(2, 'Fin - Victoire du CMC', 'Le soulèvement populaire ne prend pas et la faible résistance conséquente à la loi conforte le gouvernement dans son action et idéologie monolingue. Il simplifie le Dictionnaire officiel à outrance, sur le modèle du Basic English (Ogden) et poursuit impunément ses exactions écologiques comme en témoigne la nouvelle Constitution.', 1, '2023-05-05 21:03:47', '2023-05-05 21:03:47'),
(3, 'Fin', 'Le soulèvement populaire s’organise de manière auto-gérée, prend de l’ampleur et parvient à renverser le pouvoir en place pour imposer une politique écolinguistique ouverte à la diversité des langues, des cultures et des espèces. \r\n\r\nMais la balkanisation des langues rend la communication de plus en plus complexe et un doute émerge : et si l’instauration d’une/de langues partagées en parallèle à la revivification de toutes les langues n’était pas une solution ? Comment résoudre la tension entre l’intercompréhension en langue universelle et le respect des langues et des cultures ?\r\n', 1, '2023-05-05 21:03:47', '2023-05-05 21:03:47'),
(4, 'The End - Victory of the popular uprising', 'R como la Vittoria! \r\n\r\nThe one that, full foule in houle, iouave carried from the keyboard ô bitumen, apicamaramages.  \r\n\r\nDies irae: hungry for oca, rutabagas and justicia, radishes and diversity, we left the Darkmachine\'s Kvern.\r\n\r\nLike One, we managed to follow the lost trail of the Apokalypsens bank, to the Global Seed Vault, in the Svaltad Archipelago, where all the seeds of the agriKôl world were kept, where 15,000 years of seeds were preserved.\r\n\r\nQue tesouro inestimável: decades of agricultural treasures amassed, lost and forgotten under the permafrost after the Third World War. Alone, we didn\'t have the words to track them down.  Together and polyphone, we were fuerza viva: we made the revolution from dreams to seeds.  Thanks to you, seeds sown in all winds during our victorious return expedition will regrow all the world\'s plants: chickpeas from Iraq, potatoes from Peru, wheat from Berry... On the ruins of the devastated CMC, a cornucopia and espera are already growing amidst the wild grasses. The river of dulce water and unfettered words now irrigates our unbridled lands.\r\n\r\nTime will make all appetites blossom again, without exhausting the land to fill the purses of the monopolies.\r\n\r\nThanks to ancestral knowledge coiled in the heart of revitalized languages, we will once again cultivate medicinal plants. The sick will heal.\r\n\r\nFree to be yourselves in your languages, calm your anger: some are already swapping weapons for spades and rakes. A garden of soil and air is already emerging from the earth, at the dawn of summer.\r\n\r\nFree to be yourselves in your languages, calm your anger: some are already swapping weapons for spades and rakes. A garden of soil and air is already emerging from the earth, at the dawn of summer.\r\nTurn off your machine and deuz, viene, come, in all your languages, to cultivate and babble our Like-One garden: let Babel dream!\r\n', 1, '2023-05-05 21:03:47', '2023-05-05 21:03:47'),
(5, 'The End - Victory of the CMC', 'The grassroots uprising failed to materialize and the resulting lack of resistance to the law reinforced the government\'s monolingual approach and ideology. It over-simplified the Official Dictionary along the lines of \"Basic English\" (Ogden) and continued its ecological abuses with impunity, as evidenced by the new Constitution.', 1, '2023-05-05 21:03:47', '2023-05-05 21:03:47'),
(6, 'The End', 'The grassroots uprising was self-organized, gained momentum and succeeded in overthrowing the ruling power to impose an ecolinguistic policy open to the diversity of languages, cultures and species. \r\n\r\nBut the balkanization of languages is making communication more and more complex, and a doubt is emerging: what if the establishment of one or more shared languages in parallel with the revival of all languages were not a solution? How can we solve the tension between intercomprehension in a universal language and respect for languages and cultures?\r\n\r\n', 1, '2023-05-05 21:03:47', '2023-05-05 21:03:47');

-- --------------------------------------------------------

--
-- Structure de la table `Examples`
--

CREATE TABLE IF NOT EXISTS `Examples` (
  `id` int NOT NULL,
  `author` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` longtext,
  `decree` int DEFAULT NULL,
  `belief` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `Examples`
--

INSERT INTO `Examples` (`id`, `author`, `title`, `text`, `decree`, `belief`, `type`) VALUES
(1, 'Potato', 'Faire patate', 'J’en ai gros sur la patate… La disparition de ces noms de plantes, et des plantes elles-mêmes, me désespère. Tout un chacun, qu’il s’agisse de Monsieur ou de Madame Patate, devrait réagir de même. Ne soyons pas des couch potatoes, rebellons-nous pour que ce décret devienne une patate chaude entre les mains du CMC ! La meilleure façon de se rebeller pourrait être de semer à tous vents…', 1, 'against', 'contribution'),
(2, 'Sibylline Dangers', 'Épuration écologique et linguistique', 'Tilleul, courge, chêne, orchidée, pommier, haricot, cerisier, platane… Et maintenant « patate » ! Cette fois, ces damnés linguistes du Centre de la Monoculture ont même banni « patate » du dernier opus du Dictionnaire officiel de la CMC : nous ne dînerons plus, frugaux et indifféremment, que de purées de légumes. Et encore, seulement les grands jours : d’un fruit en dessert auquel sera éventuellement accolé un adjectif pour en caractériser la taille, la forme ou la couleur. Moi, ce soir, comme tous les autres soirs, je mangerai les yeux caves devant mon écran une soupe de gros légumes et un fruit rond. Austère. Comme le goût fade de ces succédanés issus de croisements in vitro : Nutriscore imbattable, évidemment. Ils avaient commencé par les arbres. Déjà, lors du Coup d’Etat du CMC, personne n’avait bronché de voir rayé du dictionnaire le cocobolo ou le pokemoboy, moi la première : si peu savaient encore à quelles espèces ils s’attachaient dans leur verdoyante jeunesse puisque les mots pour les dire étaient si rarement usités. À quoi bon ? Qui en avait déjà vu, de toute façon des cocolos et des pokemboys ? Vous, oui ?  Moi, non, jamais. Ça avait certes jasé un peu, lorsqu’ils avaient refrancisé le baobab et l’araucaria, en bobe et en aroque. Les autres plantes aux noms étrangers avaient suivi : le pomel, le curcume, le dèkon, le chou patchoix, la rutabague… Ca, c\'était avant qu’ils ne disparaissent tout bonnement des étals. Trop coûteux, trop difficiles à faire pousser, pas assez performants énergétiquement, par rapport à leurs légumes de laboratoire et leur agriculture intensive.\r\n\r\n\r\nJusqu’où iront-ils nous rogner la nature ? Jusqu’où accepterons-nous cette stérilisation du monde si ample qu’a été notre langue ? Jusqu’où ? Il faut agir ! Tant d’espèces végétales ont déjà disparu. N’existent-elles plus que dans de lointains souvenirs ? Si seulement des esprits clairvoyants en avaient gardé des semences, des semences du monde entier : alors il serait possible, peut-être, de les faire germer à nouveau sous toutes les latitudes ! Mais qui aurait pu imaginer… Et quelle banque aura été assez folle pour contenir un tel rêve en graines ?', 1, 'against', 'contribution'),
(4, 'LEEL', 'Toutes langues dehors', 'Something esta pasando ! C’est encore incertain, encore subterraneo, mais des voix s’élèvent au sein même de la Machine officielle, pas même sur la DarkMachine où j’écris, peu héroïque, en samizdat, ces carnets sin lector. Des voix qui résistent contre cette épuration linguistique, in French dans le texte, et en des langues dont I never have même entendu parler. Existent-elles, ont-elle déjà existé, todas ? J’ai navigué la nuit durant dans la Matrice, suivi les échos hypertextes de ces voix : ce sont des voyages. ', 2, 'against', 'contribution'),
(5, 'Le-Seed', 'Ça putsh', 'La révolte putsch et poursuit sa croissance. Ne bayons pas aux corneilles, alors que les racines s\'enfoncent, la tige vergrößert, se estira.. Les bourgeons s\'ouvrent, forman nuevos tiges. Så pousse !\r\nArrêtons de végéter, ne restons pas plantés là ! Soyons Lu-Seed, il faut continuer à semer, seminare, semear, sembrar… сеять, mais où trouver ces semences ?\r\n', 2, 'against', 'contribution'),
(6, 'Sibylline Dangers', 'ma Babel et re-belle', 'Belle, ma Babel, et re-belle ! Deux fois jaillie de la tierra, telle Pachamana’s mamelles, et deux fois (abat)tu(e). \r\nLa Tour, effondrée. La bibliothèque ? Brûlée.\r\nPourtant nous n\'avons pas dit notre dernier mot. Ce mot, ce sera quoi ? Ce sera toi, you there, te, ti, أنت. Ce sera trois. Oui : tri ! Three ! три ! Tria ! Troie. Une troisième fois, nous ferons fondation unie vers Celle. Babil, que ton germe revienne sur le terreau rendu si infertile : nous referons polyculture.\r\nInfertile ? አይኮንን ! Sous la chape en béton de la langue inique tressaillent encore, la sève aux lèvres, mille voix inaudibles. Que disent-elles ? What do you say? Petra lavares ? Tes mémoires, tus promisas… Et bien davantage. Peuple muté muti, il n\'est pas encore trop tard. Nous avons retrouvé le chemin d’un verger vieux de milliers d’année. Il pousse encore ici : 78.2356185913086,15.491536140441895.\r\nBabille-moi, Babil, peinturle en façade ou fractale.\r\nHurle, toutes langues dehors et toute honte lue. Je suis là, je t\'écoute, je serai l’architexte. Écrilis en la langue tienne ! Fasse que notre aleph monument aux mots à vif, refonde labile et notre bibliothèque et notre jardin. Nous y bâtirons notre maison nouvelle, en diverses cités.\r\nMa langue se délite, dekale, s’écaille comme un œuf trop cuit, es desfà en mila pieza qui refondent la vie en pixels. Il ne dépend que de vous, Бабел mon amour, sweet Babɛli, o meu ❤️ , que la infano que nous porterons ait ta langue, tes yeux, et qu’il grandisse à l’ombre de pommiers en fleurs.', 3, 'against', 'contribution'),
(7, 'Semi-Rebel', 'Troll au Nord', 'Oui Sibylline, Troie, Trois, Tre,Три, 三个, 세, ثلاث, ce mot je le fais mien. Partons en guerre contre la langue inique qu\'on veut nous imposer. Hélène aux grandes Tres, inspire-moi et ouvre le chemin jusqu’aux lointains archipels.\r\nTrollons le système et faisons le Tri parmi nos amis. Nous partirons à Trois, mais par un prompt renfort, serons bientôt Trois mille, arriverons à bon port ! Eins Zwei Drei nous serons la pensée, la parole et l\'action dans le grand Nord.\r\nUno dos tres nous incarnerons la naissance, la vie et la mort.\r\n一二三  nous viserons la diversity del mundo !\r\n3 c\'est la fécondité, la réunion qui permet la naissance d’un être nouveau…\r\nDes langues en germes ! Lenguas en construcción ! \r\nViendrez-vous avec nous ? Qu’emporterez-vous dans vos poches au sortir de la cathédrale de glace : <a href=\"https://virtualtourcompany.co.uk/GlobalSeedVault/index.html#popup_7737\" target=\"_blank\">le réservoir mondial de semences de Svalbard</a> ? Quelle trouvaille ? Quelle semaille ?', 3, 'against', 'contribution'),
(9, 'Coach Potato', 'Potato', 'The disappearance of these plant names, and of the plants themselves, makes me despair. Everyone, whether Mr. or Mrs. Potato, should react in the same way. Let\'s not be couch potatoes, let\'s revolt so that this decree becomes a hot potato in the hands of the CMC! The best way to rebel might be to sow to all winds...', 1, 'against', 'contribution'),
(10, 'Sibylline Dangers', 'Ecological and linguistic purification', 'Basswood, squash, oak, orchid, apple tree, bean, cherry tree, plane tree... And now \"potato\"! This time, those damned linguists at the Center of Monoculture have even banned  \"potato \" from the latest edition of the CMC Official Dictionary: we will now only dine, frugal and indifferent, on anything but vegetable purees. \r\nAnd, only on special days: a fruit for dessert, possibly with an adjective to describe its size, shape or colour. Tonight, like every other night, I will eat a soup of large vegetables and a round fruit with my eyes glazed over in front of my screen. Austere. Like the bland taste of these in vitro crossbred substitutes: Unbeatable Nutriscore, of course. They started with the trees. Already, during the Rebellion, no one had flinched at seeing cocobolo or pokemoboy stricken from the dictionary, me first: so few still knew what species they were attached to in their verdant youth since the words to say them were so rarely used. Why bother? Who had ever seen cocolos and pokemboys anyways ? Have you ? I never did. There had indeed been some chatter, when they had refrancised the baobab and araucaria into bobe and aroque. Other plants with foreign names followed: pomel, curcume, dèkon, patchoix cabbage, rutabagas... That was before they simply disappeared from the shelves. Too expensive, too difficult to grow, not energetically efficient enough, compared to their laboratory vegetables and intensive farming. \r\n\r\n\r\nHow far will they go to cut down our nature ? How far will we accept this sterilisation of the world, which has been so extensive as our language ? How far? We must act! So many plant species have already disappeared. Do they now exist only in distant memories? If only some far-sighted people had saved some seeds, seeds from all over the world: then it might be possible to make them germinate again in every latitude! But who could have imagined... And what bank would have been foolish enough to contain such a dream in seeds?\r\n', 1, 'against', 'contribution'),
(11, 'Elit', 'Translanguaging', 'Something esta pasando ! It\'s still uncertain, still subterraneo, but voices are being raised even within the official Machine, not even on the DarkMachine where I write, unheroically, in samizdat, these sin lector notebooks. Voices resisting this linguistic cleansing, in English in the text, and in languages I\'ve never even heard of. Do they exist, have they ever existed, todas? I\'ve been surfing the Matrix all night, following the hypertext echoes of these voices: these are journeys. \r\nSome 1st posts have been censored by the algorithm, but others have managed to break the code, peppering themselves with banned words, in unauthorized English, and in todas las languas of the Word. It\'s rumbling, en-ô-du-code. Some are even calling for bank robberies! Not the banks of the CMC, no: a strange Bank, of Apocalypse, with seeds and sowing full of coffers.\r\nGo and read it for yourself!\r\n', 2, 'against', 'contribution'),
(12, 'Le-Seed', 'Ça putsh', 'The putsch revolt continues to grow. Don\'t let the crows bite, as the roots sink, the stem vergrößert, se estira... Buds open, forman nuevos stems. Så pousse!\r\n Let\'s stop vegetating, let\'s not just stand there! Let\'s be Lu-Seed, we must keep on sowing, seminare, semear, sembrar... сеять, but where do we find these seeds?\r\n', 2, 'against', 'contribution'),
(13, 'Sibylline Dangers', 'My rebel Babel', 'Belle, my Babel, and re-belle! Twice sprung from the earth, like Pachamana\'s teats, and twice killed. \r\nThe Tower, collapsed. The library? Burnt.\r\nYet we haven\'t said our last word. What will that word be? It will be you, toi là, te, ti, أنت. It will be three. Yes: three! три! Tria! Trois ! Troy. A third time, we\'ll make a united foundation towards Celle. Babil, let your germ return to the soil that has become so infertile: we\'ll do polyculture again.\r\n\r\nInfertile? አይኮንን! Beneath the concrete screed of iniquitous language, a thousand inaudible voices still twitch with sap in their lips. What do they say? What do you say? Petra lavares? Your memories, tus promisas... And much more. Mute people, it\'s not too late yet. We\'ve found our way back to an orchard thousands of years old. It still grows here: 78.2356185913086,15.491536140441895.\r\n\r\nBabille-me, Babil, paint in facade or fractal.\r\n\r\nScream, all tongues out and all shame read. I\'m here, I\'m listening, I\'ll be the architect. Write in your own language! May our aleph monument to words in the flesh, refound our library and our garden. We\'ll build our new house, in various cities.\r\n\r\nMy language is crumbling, peeling, flaking like an overcooked egg, es desfà en mila pieza that refound life in pixels. It only depends on you, Бабел my love, sweet Babɛli, o meu ❤️ , that the infano we will bear has your tongue, your eyes, and that it grows in the shade of blossoming apple trees.\r\n', 3, 'against', 'contribution'),
(14, 'Semi-Rebel', 'Troll in the North', 'Yes Sibylline, Three, Trois, Troy, Tre,Три, 三个, 세, ثلاث, this word I make mine. Let\'s go to war against the iniquitous language they want to impose on us. Helene of the great Tres, inspire me and open the way to distant archipelagos.\r\n\r\nTroll the system and sort out our friends. We\'ll leave in threes, but with swift reinforcement, we\'ll soon be Three thousand, and we\'ll arrive safely! Eins Zwei Drei we\'ll be thinking, speaking and acting in the far north.\r\n\r\nUno dos tres we will embody birth, life and death.\r\n\r\n一二三 we aim for diversity del mundo!\r\n\r\n3 is fecundity, the reunion that gives birth to a new creation…\r\n\r\nLanguages in the making! Lenguas en construcción! \r\n\r\nWill you come with us? What will you be carrying in your pockets when you leave the https://virtualtourcompany.co.uk/GlobalSeedVault/index.html#popup_7737 ice cathedral? \r\nWhat will you find? What seeds?\r\n', 3, 'against', 'contribution');

-- --------------------------------------------------------

--
-- Structure de la table `SequelizeMeta`
--

CREATE TABLE IF NOT EXISTS `SequelizeMeta` (
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

--
-- Déchargement des données de la table `SequelizeMeta`
--

INSERT INTO `SequelizeMeta` (`name`) VALUES
('20230424112545-create-node.js'),
('20230424113328-update-nodes.js'),
('20230424194139-node-update.js'),
('20230424201030-node-fix.js'),
('20230424201536-node-fix.js'),
('20230424202550-node-fix.js'),
('20230503141455-create-user.js'),
('20230503182134-create-session-info.js'),
('20230505172658-create-decree.js'),
('20230505172851-create-example.js'),
('20230505202705-create-end.js'),
('20230514221523-node-change-type.js'),
('20230518135144-node_text-change-type.js');

-- --------------------------------------------------------

--
-- Structure de la table `session-67s`
--

CREATE TABLE IF NOT EXISTS `session-67s` (
  `id` int NOT NULL,
  `author` varchar(255) DEFAULT NULL,
  `text` longtext,
  `react` int DEFAULT NULL,
  `belief` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `session-67s`
--

INSERT INTO `session-67s` (`id`, `author`, `text`, `react`, `belief`, `title`, `type`) VALUES
(1, 'CMC', 'Merci de respecter nos décrets', NULL, NULL, 'Centre de la Monoculture', 'root'),
(2, 'CMC', 'Il est désormais interdit d\'utiliser des hyponymes de « fleurs », d’« arbre » ou de « légume », comme pâquerette, mimosa, magnolia, artichaut, potiron, pomme de terre ou patate... Les termes scientifiques pour décrire une fleur, une plante ou un arbre sont bannis en raison de leur inutilité suite à la disparition de ces végétaux et des risques de mécompréhension. Devront être utilisées des expressions génériques et descriptives comme « fleur rose », « grand/petit arbre », « légume vert » ou « petit légume rond ».\r\nCes usages linguistiques sont évidemment corrélés à une culture agroalimentaire privilégiant la monoculture des fruits et légumes résistants et offrant de bons rendements pour nourrir le plus grand nombre.\r\nTout usage linguistique et cultural non conventionnel sera sanctionné. -- Le CMC\r\n', 1, NULL, 'Décret sur les plantes', 'decree'),
(3, 'Potato', 'J’en ai gros sur la patate… La disparition de ces noms de plantes, et des plantes elles-mêmes, me désespère. Tout un chacun, qu’il s’agisse de Monsieur ou de Madame Patate, devrait réagir de même. Ne soyons pas des couch potatoes, rebellons-nous pour que ce décret devienne une patate chaude entre les mains du CMC ! La meilleure façon de se rebeller pourrait être de semer à tous vents…', 2, 'against', 'Faire patate', 'contribution'),
(4, 'Sibylline Dangers', 'Tilleul, courge, chêne, orchidée, pommier, haricot, cerisier, platane… Et maintenant « patate » ! Cette fois, ces damnés linguistes du Centre de la Monoculture ont même banni « patate » du dernier opus du Dictionnaire officiel de la CMC : nous ne dînerons plus, frugaux et indifféremment, que de purées de légumes. Et encore, seulement les grands jours : d’un fruit en dessert auquel sera éventuellement accolé un adjectif pour en caractériser la taille, la forme ou la couleur. Moi, ce soir, comme tous les autres soirs, je mangerai les yeux caves devant mon écran une soupe de gros légumes et un fruit rond. Austère. Comme le goût fade de ces succédanés issus de croisements in vitro : Nutriscore imbattable, évidemment. Ils avaient commencé par les arbres. Déjà, lors du Coup d’Etat du CMC, personne n’avait bronché de voir rayé du dictionnaire le cocobolo ou le pokemoboy, moi la première : si peu savaient encore à quelles espèces ils s’attachaient dans leur verdoyante jeunesse puisque les mots pour les dire étaient si rarement usités. À quoi bon ? Qui en avait déjà vu, de toute façon des cocolos et des pokemboys ? Vous, oui ?  Moi, non, jamais. Ça avait certes jasé un peu, lorsqu’ils avaient refrancisé le baobab et l’araucaria, en bobe et en aroque. Les autres plantes aux noms étrangers avaient suivi : le pomel, le curcume, le dèkon, le chou patchoix, la rutabague… Ca, c\'était avant qu’ils ne disparaissent tout bonnement des étals. Trop coûteux, trop difficiles à faire pousser, pas assez performants énergétiquement, par rapport à leurs légumes de laboratoire et leur agriculture intensive.\r\n\r\nJusqu’où iront-ils nous rogner la nature ? Jusqu’où accepterons-nous cette stérilisation du monde si ample qu’a été notre langue ? Jusqu’où ? Il faut agir ! Tant d’espèces végétales ont déjà disparu. N’existent-elles plus que dans de lointains souvenirs ? Si seulement des esprits clairvoyants en avaient gardé des semences, des semences du monde entier : alors il serait possible, peut-être, de les faire germer à nouveau sous toutes les latitudes ! Mais qui aurait pu imaginer… Et quelle banque aura été assez folle pour contenir un tel rêve en graines ?', 2, 'against', 'Épuration écologique et linguistique', 'contribution'),
(5, 'CMC', 'Suite à la prolifération d’usages linguistiques non conventionnels, le CMC rappelle que la Langue commune est la seule langue de la République autorisée.\r\nLa simplicité et l’intercompréhension étant les objectifs principaux du CMC, l’emploi dans la sphère publique, professionnelle et privée d’une langue autre que la langue commune est désormais formellement interdit. Cette interdiction s\'étend à tous les patois. Les termes inventés ou les usages de la langue non conforme au règlement syntaxique et orthographique, comme le verlan ou l’écriture phonétique sont aussi fermement prohibés. Ces pratiques excluent en effet des communications les personnes qui ne peuvent les comprendre.\r\nPour le bien commun, utilisons une seule langue expurgée de mots d’origine étrangère, gage de paix, d’unité et d\'intercompréhension, tout comme nous soutenons la monoculture qui simplifie la pratique culturale et augmente la production pour le bénéfice commun.\r\nTout usage linguistique et cultural non conventionnel sera sanctionné. -- Le CMC\r\n\r\n', 1, NULL, 'Décret sur les langues', 'decree'),
(6, 'LEEL', 'Something esta pasando ! C’est encore incertain, encore subterraneo, mais des voix s’élèvent au sein même de la Machine officielle, pas même sur la DarkMachine où j’écris, peu héroïque, en samizdat, ces carnets sin lector. Des voix qui résistent contre cette épuration linguistique, in French dans le texte, et en des langues dont I never have même entendu parler. Existent-elles, ont-elle déjà existé, todas ? J’ai navigué la nuit durant dans la Matrice, suivi les échos hypertextes de ces voix : ce sont des voyages. ', 5, 'against', 'Toutes langues dehors', 'contribution'),
(7, 'Le-Seed', 'La révolte putsch et poursuit sa croissance. Ne bayons pas aux corneilles, alors que les racines s\'enfoncent, la tige vergrößert, se estira.. Les bourgeons s\'ouvrent, forman nuevos tiges. Så pousse !\r\nArrêtons de végéter, ne restons pas plantés là ! Soyons Lu-Seed, il faut continuer à semer, seminare, semear, sembrar… сеять, mais où trouver ces semences ?\r\n', 5, 'against', 'Ça putsh', 'contribution'),
(9, 'Tomate', 'Rougissez, tomates !\nTous les petits légumes ronds et rouges pourris sont bons à jeter sur les grosses légumes du CML. \n', 2, 'against', 'Cyberaid tomatesque sur le décret', 'contribution'),
(10, 'a', 'a', 2, 'against', 'a', 'contribution'),
(11, 'CMC', 'Des études scientifiques ont démontré le nombre élevé de termes polysémiques, susceptibles de qualifier deux réalités diverses : ces mots à double sens nuisent à la clarté de la compréhension et à l’efficacité de la communication. Les jeux de mots humoristiques sont proscrits. De même, les textes littéraires deviennent illégaux, en raison du recours à des qualificatifs désuets (caustique, acrimonieux, affable, prévenant…) autres que ceux de la liste des adjectifs recommandés (liste consultable sur le site du CMC), tels que gentil, méchant, bon, mauvais.\r\nPlus encore, toute métaphore florale ou végétale est proscrite en raison de leur caractère incitatif à la polyculture, dangereuse pour le bien commun.\r\nTout usage linguistique et cultural non conventionnel sera sanctionné. -- Le CMC\r\n', 1, NULL, 'Décret sur la littérature', 'decree'),
(12, 'Sibylline Dangers', 'Belle, ma Babel, et re-belle ! Deux fois jaillie de la tierra, telle Pachamana’s mamelles, et deux fois (abat)tu(e). \r\nLa Tour, effondrée. La bibliothèque ? Brûlée.\r\nPourtant nous n\'avons pas dit notre dernier mot. Ce mot, ce sera quoi ? Ce sera toi, you there, te, ti, أنت. Ce sera trois. Oui : tri ! Three ! три ! Tria ! Troie. Une troisième fois, nous ferons fondation unie vers Celle. Babil, que ton germe revienne sur le terreau rendu si infertile : nous referons polyculture.\r\nInfertile ? አይኮንን ! Sous la chape en béton de la langue inique tressaillent encore, la sève aux lèvres, mille voix inaudibles. Que disent-elles ? What do you say? Petra lavares ? Tes mémoires, tus promisas… Et bien davantage. Peuple muté muti, il n\'est pas encore trop tard. Nous avons retrouvé le chemin d’un verger vieux de milliers d’année. Il pousse encore ici : 78.2356185913086,15.491536140441895.\r\nBabille-moi, Babil, peinturle en façade ou fractale.\r\nHurle, toutes langues dehors et toute honte lue. Je suis là, je t\'écoute, je serai l’architexte. Écrilis en la langue tienne ! Fasse que notre aleph monument aux mots à vif, refonde labile et notre bibliothèque et notre jardin. Nous y bâtirons notre maison nouvelle, en diverses cités.\r\nMa langue se délite, dekale, s’écaille comme un œuf trop cuit, es desfà en mila pieza qui refondent la vie en pixels. Il ne dépend que de vous, Бабел mon amour, sweet Babɛli, o meu ❤️ , que la infano que nous porterons ait ta langue, tes yeux, et qu’il grandisse à l’ombre de pommiers en fleurs.', 11, 'against', 'ma Babel et re-belle', 'contribution'),
(13, 'Semi-Rebel', 'Oui Sibylline, Troie, Trois, Tre,Три, 三个, 세, ثلاث, ce mot je le fais mien. Partons en guerre contre la langue inique qu\'on veut nous imposer. Hélène aux grandes Tres, inspire-moi et ouvre le chemin jusqu’aux lointains archipels.\r\nTrollons le système et faisons le Tri parmi nos amis. Nous partirons à Trois, mais par un prompt renfort, serons bientôt Trois mille, arriverons à bon port ! Eins Zwei Drei nous serons la pensée, la parole et l\'action dans le grand Nord.\r\nUno dos tres nous incarnerons la naissance, la vie et la mort.\r\n一二三  nous viserons la diversity del mundo !\r\n3 c\'est la fécondité, la réunion qui permet la naissance d’un être nouveau…\r\nDes langues en germes ! Lenguas en construcción ! \r\nViendrez-vous avec nous ? Qu’emporterez-vous dans vos poches au sortir de la cathédrale de glace : <a href=\"https://virtualtourcompany.co.uk/GlobalSeedVault/index.html#popup_7737\" target=\"_blank\">le réservoir mondial de semences de Svalbard</a> ? Quelle trouvaille ? Quelle semaille ?', 11, 'against', 'Troll au Nord', 'contribution'),
(14, 'someone', 'writting in english...', 7, 'in_favor', 'In english', 'contribution'),
(15, 'b', 'b', 10, 'in_favor', 'b', 'contribution'),
(16, 'c', 'c', 15, 'neutral', 'c', 'contribution'),
(17, 'g', 'ss', 10, 'neutral', 'test', 'contribution'),
(18, 'quelqu\'un', 'quelqu\'un arrive...', 13, 'against', 'goblin du sud', 'contribution'),
(19, 'test', 'test', 13, 'against', 'test', 'contribution'),
(20, 'Essai', 'Essai', 2, 'against', 'Essai', 'contribution'),
(21, 'suite', 'suite', 18, 'in_favor', 'suite', 'contribution');

-- --------------------------------------------------------

--
-- Structure de la table `session-74s`
--

CREATE TABLE IF NOT EXISTS `session-74s` (
  `id` int NOT NULL,
  `author` varchar(255) DEFAULT NULL,
  `text` longtext,
  `react` int DEFAULT NULL,
  `belief` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `session-74s`
--

INSERT INTO `session-74s` (`id`, `author`, `text`, `react`, `belief`, `title`, `type`) VALUES
(1, 'CMC', 'Au service du Bien commun et de l’intercompréhension entre les peuples, le Centre de la Monoculture œuvre à la mise à jour et à l’actualisation de la Langue commune pour la mettre toujours mieux en adéquation avec l’environnement culturel et cultural. Tout usage linguistique et cultural non conventionnel sera sanctionné.  Le CMC', NULL, NULL, 'Centre de la Monoculture', 'root'),
(4, 'Myam', 'Oui, oui, oui, d\'après la conférence de Nairobi de l\'Unesco 1976.\n« La traduction favorise la compréhension entre les peuples et la coopération entre les nations » SAUF QUE\nDepuis des décennies, les théoricienNEs dénoncent la métaphore du pont comme prétendu arche de concorde entre les peuples. Le plus souvent, c\'est une entreprise de normalisation et d\'écrasement des différences. Alors décentrons ! Proliférons !', 1, 'against', 'Riposte hétérogénéisante et décentralisée', 'contribution'),
(5, 'Damasio', 'Alain Damasio, La Zone du Dehors, Paris, Gallimard, « Folio SF », 2009, p.580-581 :\ncessez de vous penser comme un petit État unifié dont votre conscience serait président ! Décentralisez vos émotions ! Laissez vos groupuscules de désirs prendre les armes et la parole ! Laissez-les filer ! […] Coupez l’autocensure, les rétrocontrôles, la fausse maîtrise ! Crevez votre sac à conscience et descendez un cran plus profond, sous l’individu que vous croyez être, jusqu’aux mouvements qui vous font ! Et vous n’y perdrez pas votre identité, au contraire, vous l’atteindrez dans ce que ses puissances ont d’unique ! Nous portons tous en nous des zoos intimes dont il faut ouvrir les cages ! Libérez le moineau, le tigre pourpre, libérez le rhino et les éléphants, libérez les singes, l’ours et le lynx qui sont en vous !', 1, 'against', 'ibérez les singes, l’ours et le lynx qui sont en vous !', 'contribution'),
(6, 'L\'homme qui cherche le vrai', 'Nous pourrons les controler.', 4, 'in_favor', 'Surveillons ces déviances.', 'contribution'),
(7, 'CMC', 'Il est désormais interdit d\'utiliser des hyponymes de « fleurs », d’« arbre » ou de « légume », comme pâquerette, mimosa, magnolia, artichaut, potiron, pomme de terre ou patate... Les termes scientifiques pour décrire une fleur, une plante ou un arbre sont bannis en raison de leur inutilité suite à la disparition de ces végétaux et des risques de mécompréhension. Devront être utilisées des expressions génériques et descriptives comme « fleur rose », « grand/petit arbre », « légume vert » ou « petit légume rond ».\r\nCes usages linguistiques sont évidemment corrélés à une culture agroalimentaire privilégiant la monoculture des fruits et légumes résistants et offrant de bons rendements pour nourrir le plus grand nombre.\r\nTout usage linguistique et cultural non conventionnel sera sanctionné. -- Le CMC\r\n', 1, NULL, 'Décret sur les plantes', 'decree'),
(8, 'Potato', 'J’en ai gros sur la patate… La disparition de ces noms de plantes, et des plantes elles-mêmes, me désespère. Tout un chacun, qu’il s’agisse de Monsieur ou de Madame Patate, devrait réagir de même. Ne soyons pas des couch potatoes, rebellons-nous pour que ce décret devienne une patate chaude entre les mains du CMC ! La meilleure façon de se rebeller pourrait être de semer à tous vents…', 7, 'against', 'Faire patate', 'contribution'),
(9, 'Sibylline Dangers', 'Tilleul, courge, chêne, orchidée, pommier, haricot, cerisier, platane… Et maintenant « patate » ! Cette fois, ces damnés linguistes du Centre de la Monoculture ont même banni « patate » du dernier opus du Dictionnaire officiel de la CMC : nous ne dînerons plus, frugaux et indifféremment, que de purées de légumes. Et encore, seulement les grands jours : d’un fruit en dessert auquel sera éventuellement accolé un adjectif pour en caractériser la taille, la forme ou la couleur. Moi, ce soir, comme tous les autres soirs, je mangerai les yeux caves devant mon écran une soupe de gros légumes et un fruit rond. Austère. Comme le goût fade de ces succédanés issus de croisements in vitro : Nutriscore imbattable, évidemment. Ils avaient commencé par les arbres. Déjà, lors du Coup d’Etat du CMC, personne n’avait bronché de voir rayé du dictionnaire le cocobolo ou le pokemoboy, moi la première : si peu savaient encore à quelles espèces ils s’attachaient dans leur verdoyante jeunesse puisque les mots pour les dire étaient si rarement usités. À quoi bon ? Qui en avait déjà vu, de toute façon des cocolos et des pokemboys ? Vous, oui ?  Moi, non, jamais. Ça avait certes jasé un peu, lorsqu’ils avaient refrancisé le baobab et l’araucaria, en bobe et en aroque. Les autres plantes aux noms étrangers avaient suivi : le pomel, le curcume, le dèkon, le chou patchoix, la rutabague… Ca, c\'était avant qu’ils ne disparaissent tout bonnement des étals. Trop coûteux, trop difficiles à faire pousser, pas assez performants énergétiquement, par rapport à leurs légumes de laboratoire et leur agriculture intensive.\r\n\r\n\r\nJusqu’où iront-ils nous rogner la nature ? Jusqu’où accepterons-nous cette stérilisation du monde si ample qu’a été notre langue ? Jusqu’où ? Il faut agir ! Tant d’espèces végétales ont déjà disparu. N’existent-elles plus que dans de lointains souvenirs ? Si seulement des esprits clairvoyants en avaient gardé des semences, des semences du monde entier : alors il serait possible, peut-être, de les faire germer à nouveau sous toutes les latitudes ! Mais qui aurait pu imaginer… Et quelle banque aura été assez folle pour contenir un tel rêve en graines ?', 7, 'against', 'Épuration écologique et linguistique', 'contribution'),
(10, 'Michael Roch', 'Ma langue est un chariot allant de mon cœur à ton esprit./ Elle me déplace entier pour t’apprendre ce que je suis,/ comment je vois le monde, comment je le réfléchis./ Libre à toi d’entrer en résistance ou en communion./ Notre langue sera le reflet humble de notre relation.', 7, 'against', 'TÈ MAWON', 'contribution'),
(11, 'Sans Pratates', 'Je n\'ai jamais aimé les Bratates, bon débarras de ces mots inutiles.', 7, 'in_favor', 'BRAVO', 'contribution'),
(12, 'Huile', 'OUIIIiiii ici aussi je patate ! Patatons de concert et pas de concert !', 8, 'against', 'Ayons la frite !', 'contribution'),
(13, 'Concombre masqué', 'C\'est vraiment la fin des haricots!!!!', 9, 'against', 'C\'est la fin des haricots!', 'contribution'),
(14, 'L\'homme qui ne comprend pas', 'Et je n\'ai rien compris. Pourquoi tant de mouvement incontrôlé du clavier ?', 9, 'in_favor', 'J\'ai lu ton livre', 'contribution'),
(15, 'L\'homme qui mange', 'Respecte le Grand Décret mon petipotiron.', 13, 'in_favor', 'La din des Zaricots mon ami.', 'contribution'),
(16, 'Concombre masqué', 'on ne se dispute pas pour des queues de cerise!', 9, 'against', 'Pourquoi?', 'contribution'),
(17, 'Appfelbaum', 'Mala malus mala mala dat...', 7, 'against', 'Les racines du mal', 'contribution'),
(18, 'L\'homme qui lit vrai', 'Et je n\'ai rien compris. Pourquoi tant de mouvement incontrôlé du clavier ?', 10, 'in_favor', 'J\'ai lu ton livre', 'contribution'),
(19, 'Concombre masqué', 'J\'ai la pêche! oui j\'ai la frite et ça me donne la banane!', 7, 'against', 'ça va?', 'contribution'),
(20, 'Oui-Oui', 'Oui-Oui chantait :\n- \"Les Magnoliaaaa !\"\n-Arrête hé petittubercule, lui dit son ami Groslégumeorange\n- Laisse-moi chanter !\n- Non, tu n\'as plus le droit. Mime !\n- C\'est nul le mime, on ne peut rien dire\n- Détrompe-toi, répondit Groslégumeorange, Tu ne sais pas ce que mime osa !\n- Ah tu n\'est pas que rette ! conclut Oui-Oui', 7, 'against', 'Oui-Oui et gros lesgumes orange', 'contribution'),
(21, 'La fleur', '... la boite noire accueillera des fruits !', 18, 'against', 'Tant que le clavier bouge...', 'contribution'),
(22, 'Pâquerette fatiguée', 'Après les pesticides dans les prés, voilà qu\'ils veulent encore nous éradiquer... du dictionnaire cette fois, des bouches des amoureux qui de leurs doigts inconscients arrachent une à une nos pétales jolis pour compter leur amour et oser espérer aimer à la folie.', 15, 'against', 'Pitié ...', 'contribution'),
(23, 'L\'homme qui répond', 'Et pourquoi pas ?\nT\'aime ça les choux de brouxelles ? Le celequirit ? La crouchoute ? Hein ? Circulez les légumards.', 9, 'in_favor', 'Dehors si t\'es bien t\'y restes ', 'contribution'),
(24, 'Concombre masqué', 'Je me souviens qu\'en  allemand, pomme se disait \"Apfel\"... mais je ne sais plus si c\'était die, das ou der!', 7, 'against', 'Je me souviens...', 'contribution'),
(25, 'Bratate masquée', 'Les Bratates ne seront jamais cuites !', 11, 'against', 'Pfffff', 'contribution'),
(26, 'Сдймусштшыеу', 'Tant que le clavier bouge\nTqnt aue le clqvier bouge\nЕйте фгу ду сдймшук ищгпу', 21, 'against', 'Klavier', 'contribution'),
(27, 'acacia down under', 'comment parler du parfum sans les fleurs!\n', 7, 'against', 'Poésie', 'contribution'),
(28, 'tom', 'Rougissez, tomates ! Tous les petits légumes ronds et rouges pourris sont bons à jeter sur les grosses légumes du CML. Los. \n', 7, 'against', 'Cyberaid tomatesque sur le décret', 'contribution'),
(29, 'Concombre masqué', 'Chêne, brise tes chaines!!!!!', 1, 'against', 'libération!!!!!', 'contribution'),
(30, 'アヤメ', '一つの言語の政府に対して戦おう。世界の市民たちへ、花の名前は重要だ', 7, 'against', '戦おう', 'contribution'),
(31, 'Adolphe', 'Ne cessez de vous panser comme un petit tas unifié dont votre conscience serait le gardien de Zoo ! Centralisez vos émotions ! Délaissez vos groupes Nous portons tous en nous des Damazoos sortez vos moineau, vos tige pourpre, libérez le rhino et les éléphants, libérez les singes, l’ours et le lynx ', 5, 'in_favor', 'Alain DAMAZOO', 'contribution'),
(32, 'CMC', 'Suite à la prolifération d’usages linguistiques non conventionnels, le CMC rappelle que la Langue commune est la seule langue de la République autorisée.\r\nLa simplicité et l’intercompréhension étant les objectifs principaux du CMC, l’emploi dans la sphère publique, professionnelle et privée d’une langue autre que la langue commune est désormais formellement interdit. Cette interdiction s\'étend à tous les patois. Les termes inventés ou les usages de la langue non conforme au règlement syntaxique et orthographique, comme le verlan ou l’écriture phonétique sont aussi fermement prohibés. Ces pratiques excluent en effet des communications les personnes qui ne peuvent les comprendre.\r\nPour le bien commun, utilisons une seule langue expurgée de mots d’origine étrangère, gage de paix, d’unité et d\'intercompréhension, tout comme nous soutenons la monoculture qui simplifie la pratique culturale et augmente la production pour le bénéfice commun.\r\nTout usage linguistique et cultural non conventionnel sera sanctionné. -- Le CMC\r\n\r\n', 1, NULL, 'Décret sur les langues', 'decree'),
(33, 'LEEL', 'Something esta pasando ! C’est encore incertain, encore subterraneo, mais des voix s’élèvent au sein même de la Machine officielle, pas même sur la DarkMachine où j’écris, peu héroïque, en samizdat, ces carnets sin lector. Des voix qui résistent contre cette épuration linguistique, in French dans le texte, et en des langues dont I never have même entendu parler. Existent-elles, ont-elle déjà existé, todas ? J’ai navigué la nuit durant dans la Matrice, suivi les échos hypertextes de ces voix : ce sont des voyages. ', 32, 'against', 'Toutes langues dehors', 'contribution'),
(34, 'Le-Seed', 'La révolte putsch et poursuit sa croissance. Ne bayons pas aux corneilles, alors que les racines s\'enfoncent, la tige vergrößert, se estira.. Les bourgeons s\'ouvrent, forman nuevos tiges. Så pousse !\r\nArrêtons de végéter, ne restons pas plantés là ! Soyons Lu-Seed, il faut continuer à semer, seminare, semear, sembrar… сеять, mais où trouver ces semences ?\r\n', 32, 'against', 'Ça putsh', 'contribution'),
(35, 'L\'homme qui réapprend', 'JEU CUI DACCORD mais JGalère et toa ?', 32, 'in_favor', 'DFACON MOI JEUCPAS EKRIRE', 'contribution'),
(36, 'dage', 'ù$^)&@\"\'(', 26, 'in_favor', 'si mer', 'contribution'),
(37, 'acacia down under', 'une langue se renouvelle en permanence!', 32, 'against', 'renouvellement', 'contribution'),
(38, 'courgette masquée', 'Befreie unseren Freund \"le concombre masqué\"', 32, 'against', 'was?', 'contribution'),
(39, 'YESMAN', 'YES YES YES YES YES YES YES YES !', 32, 'in_favor', 'YES', 'contribution'),
(40, 'qui vous aime', 'DAMAZOO zêt le plu bô', 31, 'against', 'Une fan', 'contribution'),
(41, '?', '????????????????????????????????????????????', 32, 'against', '? ', 'contribution'),
(42, 'courgette masquée', 'Die mehrsprachigen Menschen werden gewinnen\nmonikieliset ihmiset voittavat\nbato oyo balobaka minɔkɔ mingi bakolonga\nկհաղթի բազմալեզու ժողովուրդը', 32, 'against', 'le peuple plurilingue vaincra', 'contribution'),
(43, 'La caméra', '<écran noir>', 6, 'against', 'ON TE VOIT', 'contribution'),
(44, 'C\'est celui qui le dit qui laid', 'Quand on peut prophétiser la vie du mot et du mort, pourquoi vouloir déjouer ce qui est pour un futur incertain ? Comme disaient les anciens, un tien vaut mieux deux tu l\'auras mon loustic.', 32, 'in_favor', 'Temps mort, tant de morts', 'contribution'),
(45, 'CMCiste', 'Pourquoi vouloir du quiproquo, de l\'ambiguïté: un mot-une chose, on se comprendra mieux. Pensons à nos enfants! Il faut leur simplifier la tâche. Il y a des vrais défis.', 1, 'in_favor', 'Bien sûr', 'contribution'),
(46, 'C', 'À chacun son langage', 32, 'against', 'tösabit', 'contribution'),
(47, 'Fraises', 'Non, no, no, nein, nich, oxi, nem, nee, nul', 39, 'against', 'non', 'contribution'),
(48, 'L\'abbé Grégoire', 'Soutien total au CMC ! Poursuivons cette œuvre civilisatrice !', 32, 'in_favor', 'Espéranto-mondialisme', 'contribution'),
(49, 'Crêt', 'Le décret décrée. NO décret ', 32, 'against', 'décret sur les décrets', 'contribution'),
(50, 'DamanaZoo', 'Damazoo ti ringrazia !', 40, 'against', 'Bô Damnazoo', 'contribution'),
(51, 'Qu\'on les enferme', 'On va les mettre avec tous les déviants qui trainent sur les réseaux ca nous fera des vacances. En orbite direct. Hop.', 7, 'in_favor', 'ILS SONT OU LES PLATISTES', 'contribution'),
(52, 'courgette masquée', 'non au monolinguisme et à la pensée unique!!!!!', 31, 'against', 'no passaran!', 'contribution'),
(53, 'kanet', 'kanet kanet verjelennig, kanet in alle Sprachen', 32, 'against', 'chant des dissidents', 'contribution'),
(54, 'Fraises', 'Si la langue est tellement claire et transparente est-ce que vous pouvez me dire quelle est la couleur de ma culotte? ', 43, 'against', 'bugyi', 'contribution'),
(55, 'JULES FERRY', 'Deux trois règles, deux trois coups de règles, et aux oubliettes les néogauchistes.', 1, 'in_favor', 'L\'homme qui tape', 'contribution'),
(56, 'Saipas', 'tasréZon monpaute !\n6 tu fait 1 et fort ti arrive', 35, 'against', 'ckwoissa?', 'contribution'),
(57, 'Nihongo girl', 'To write in only one langague would prevent misunderstanding ? Oh Vraiment ? En quoi est-ce que je dérange lorsque j\'emploie des termes que mes parents utilisaient avant moi ? Ils étaient compris et comprenaient. たまに、フランス語の言葉が足りへん。', 32, 'against', 'longue vie au monolinguism', 'contribution'),
(58, 'diabolos', 'et inversement\net inverse ment !', 29, 'against', 'renversant', 'contribution'),
(59, 'keyboard', 'Parce que j\'ai le clavier qui me démange lorsque l\'on m\'interdit de m\'exprimer', 7, 'against', 'le clavier qui me démange', 'contribution'),
(60, 'ohne', 'Genre tes darons te comprennent LOL. PLS. ', 54, 'against', 'darons', 'contribution'),
(61, 'CMC', 'Des études scientifiques ont démontré le nombre élevé de termes polysémiques, susceptibles de qualifier deux réalités diverses : ces mots à double sens nuisent à la clarté de la compréhension et à l’efficacité de la communication. Les jeux de mots humoristiques sont proscrits. De même, les textes littéraires deviennent illégaux, en raison du recours à des qualificatifs désuets (caustique, acrimonieux, affable, prévenant…) autres que ceux de la liste des adjectifs recommandés (liste consultable sur le site du CMC), tels que gentil, méchant, bon, mauvais.\r\nPlus encore, toute métaphore florale ou végétale est proscrite en raison de leur caractère incitatif à la polyculture, dangereuse pour le bien commun.\r\nTout usage linguistique et cultural non conventionnel sera sanctionné. -- Le CMC\r\n', 1, NULL, 'Décret sur la littérature', 'decree'),
(62, 'Sibylline Dangers', 'Belle, ma Babel, et re-belle ! Deux fois jaillie de la tierra, telle Pachamana’s mamelles, et deux fois (abat)tu(e). \r\nLa Tour, effondrée. La bibliothèque ? Brûlée.\r\nPourtant nous n\'avons pas dit notre dernier mot. Ce mot, ce sera quoi ? Ce sera toi, you there, te, ti, أنت. Ce sera trois. Oui : tri ! Three ! три ! Tria ! Troie. Une troisième fois, nous ferons fondation unie vers Celle. Babil, que ton germe revienne sur le terreau rendu si infertile : nous referons polyculture.\r\nInfertile ? አይኮንን ! Sous la chape en béton de la langue inique tressaillent encore, la sève aux lèvres, mille voix inaudibles. Que disent-elles ? What do you say? Petra lavares ? Tes mémoires, tus promisas… Et bien davantage. Peuple muté muti, il n\'est pas encore trop tard. Nous avons retrouvé le chemin d’un verger vieux de milliers d’année. Il pousse encore ici : 78.2356185913086,15.491536140441895.\r\nBabille-moi, Babil, peinturle en façade ou fractale.\r\nHurle, toutes langues dehors et toute honte lue. Je suis là, je t\'écoute, je serai l’architexte. Écrilis en la langue tienne ! Fasse que notre aleph monument aux mots à vif, refonde labile et notre bibliothèque et notre jardin. Nous y bâtirons notre maison nouvelle, en diverses cités.\r\nMa langue se délite, dekale, s’écaille comme un œuf trop cuit, es desfà en mila pieza qui refondent la vie en pixels. Il ne dépend que de vous, Бабел mon amour, sweet Babɛli, o meu ❤️ , que la infano que nous porterons ait ta langue, tes yeux, et qu’il grandisse à l’ombre de pommiers en fleurs.', 61, 'against', 'ma Babel et re-belle', 'contribution'),
(63, 'Semi-Rebel', 'Oui Sibylline, Troie, Trois, Tre,Три, 三个, 세, ثلاث, ce mot je le fais mien. Partons en guerre contre la langue inique qu\'on veut nous imposer. Hélène aux grandes Tres, inspire-moi et ouvre le chemin jusqu’aux lointains archipels.\r\nTrollons le système et faisons le Tri parmi nos amis. Nous partirons à Trois, mais par un prompt renfort, serons bientôt Trois mille, arriverons à bon port ! Eins Zwei Drei nous serons la pensée, la parole et l\'action dans le grand Nord.\r\nUno dos tres nous incarnerons la naissance, la vie et la mort.\r\n一二三  nous viserons la diversity del mundo !\r\n3 c\'est la fécondité, la réunion qui permet la naissance d’un être nouveau…\r\nDes langues en germes ! Lenguas en construcción ! \r\nViendrez-vous avec nous ? Qu’emporterez-vous dans vos poches au sortir de la cathédrale de glace : <a href=\"https://virtualtourcompany.co.uk/GlobalSeedVault/index.html#popup_7737\" target=\"_blank\">le réservoir mondial de semences de Svalbard</a> ? Quelle trouvaille ? Quelle semaille ?', 61, 'against', 'Troll au Nord', 'contribution'),
(64, 'C', 'Je ne suis pas convaincue non plus', 38, 'neutral', 'Intéressant', 'contribution'),
(65, 'Fraises', 'comme on exprime l\'amour avec des mots', 7, 'neutral', 'mots', 'contribution'),
(66, 'L\'Homme qui dit non', 'Y en a non-oui de tous ceux qui disent que c\'est non-bien de respecter les règles STOP aux non-oui ', 61, 'in_favor', 'NON-OUI AUX NON-GENTILS', 'contribution'),
(67, 'courgette masquée', 'On s\'en moque de ton décret, patate!', 61, 'against', 'On s\'en fout!!', 'contribution'),
(68, 'Sextoy', 'On se fout où la courgette ?', 67, 'against', 'Où?', 'contribution'),
(69, 'La VOix', 'Le chemin est long et la voie étroite. Crois à la croix du CMC, oublie le CNC.', 64, 'in_favor', 'ECOUTE LA VOIX', 'contribution'),
(70, 'Emile Zolabricot', 'J\'accuse avec des mots et des haricots!', 61, 'against', 'Au bonheur des dattes', 'contribution'),
(71, 'CMCiste', 'La littérature a essayé, pas pu. Elle s\'est payé de mots. Tant pis pour elle.', 61, 'in_favor', 'Litté-rature', 'contribution'),
(72, 'Littré', 'L\'alité rature allègrement...', 7, 'against', 'Des crêts, des combes', 'contribution'),
(73, 'acacia down under', 'gentilmal! méchantbon ! jolimoche ! beaulaid ! ', 61, 'against', 'interdiction Babel', 'contribution'),
(74, 'LOL', 'Il parait que le CMC archive vos messages les mangeurs de graines. LOL.', 1, 'in_favor', 'LOL', 'contribution'),
(75, ';', 'Un dos trespassing', 52, 'against', 'Passing', 'contribution'),
(76, 'C', 'Des expériences, des rencontres', 61, 'against', 'Fluidité, mouvements', 'contribution'),
(77, 'herbe', 'braves gens braves gens\nc\'est pa smoi qu\'on rumine', 61, 'against', 'Mauvaises herbes', 'contribution'),
(78, 'Simpson', 'Des études polysémiques, susceptibles de qualifier deux réalités \ndiverses rue à double sens la nuit ont la clarté de la préhension et la cécité des cétacés. \nLes jeux deux mots sont pros. les sextes littréraires deviennent légaux, saison des retours à des calcifs désuets encaustique, cérémonieux, a-fable, pré venant… outres tout floral végétal est préscrite saison de son cratère Le Cimetière des Monstres C omiques', 61, 'against', 'Lit tes Rats Tures', 'contribution'),
(79, 'blé', 'bruyère en fleur', 7, 'against', 'grain', 'contribution'),
(80, 'Le maître d\'armes', 'Je ne mange pas de graines !!!', 74, 'against', 'Camelote', 'contribution'),
(81, 'Orwell84', 'Ce décret pousse mémé dans les orties. Utiliser des métaphores florales fait donc de moi une dissidente ? je l\'assume alors avec joie et me rebelle contre ce gouvernement ', 61, 'against', 'Welcome back to Newspeak', 'contribution'),
(82, 'en avance', 'ON A VOLE LES GRAINES ON LES DISSEMINE !!!!', 37, 'against', 'RIPOSTE SEED', 'contribution'),
(83, NULL, 'R como la Vittoria | comme-un·e !\r\n \r\nCelle que, full foule en houle, iouave portée du clavier ô bitume, apicamaramages.  \r\n \r\nDies irae : affamés d’oca, de rutabagas et de justicia, de radis et de diversity, nous avons quitté la Kverne de la Darkmachine.\r\n\r\nComme-Un·e, nous avons réussi à remonter la piste perdue de l’Apokalypsens bank, jusqu’au Global Seed Vault, dans l’Archipel de Svaltad, là où étaient conservées toutes les semences du monde agriKôl, où étaient conservés 15 000 années de semences.\r\n\r\nQue tesouro inestimável : des décennies de trésors agricoles amassés, perdus et oubliés sous le permafrost après la Troisième Guerre Mondiale. Seuls, nous n’avions pas les mots pour en trouver la trace.  Ensemble et polyphones, nous avons été fuerza viva : nous avons fait la révolution de rêves en graines.  Grâce à vous, déjà, des semailles semées à tous les vents lors de notre expédition victorieuse retour feront repousser toutes les plantes du monde : pois chiche d’Irak, patates du Pérou, blé du Berry... Sur les ruines du CMC, dévasté, poussent déjà au milieu des herbes folles une corne d’abondance et d’espera. L’onde irrigue désormais en rivière d’eau dulce et de mots disentravés, nos terres débridées.\r\n \r\nLe temps fera refleurir et nourrira tous les appétits, sans épuiser les terres pour emplir les bourses des monopuissances.\r\n\r\n Grâce aux savoirs ancestraux lovés au cœur des langues revivifiées, nous cultiverons à nouveau les plantes médicinales. Les malades guériront.\r\n\r\n Libres d’être vous-mêmes en vos langues, apaisez vos colères : déjà certains troquent les armes pour les bêches et les râteaux. Un jardin de sol y d’air sort déjà de terre, à l’orée de l’été.\r\n \r\nÉteins ta machine et deuz, viene, come, en toutes tes langues, pour cultiver et babiller notre jardin Comme-Un·e : que Babel rêve !', NULL, NULL, 'Fin - Victoire du soulèvement populaire', 'end');

-- --------------------------------------------------------

--
-- Structure de la table `session-90s`
--

CREATE TABLE IF NOT EXISTS `session-90s` (
  `id` int NOT NULL,
  `author` varchar(255) DEFAULT NULL,
  `text` longtext,
  `react` int DEFAULT NULL,
  `belief` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `session-90s`
--

INSERT INTO `session-90s` (`id`, `author`, `text`, `react`, `belief`, `title`, `type`) VALUES
(1, 'CMC', 'In the service of the common good and intercomprehension between peoples, the Monoculture Centre works to update and actualise the Common Language to bring it ever more in harmony with the cultural environment. Any unconventional linguistic or cultural practices will be penalised. -- The CMC // Au service du Bien commun et de l’intercompréhension entre les peuples, le Centre de la Monoculture œuvre à la mise à jour et à l’actualisation de la Langue commune pour la mettre toujours mieux en adéquation avec l’environnement culturel et cultural. Tout usage linguistique et cultural non conventionnel sera sanctionné.  -- Le CMC', NULL, NULL, 'CMC', 'root'),
(2, 'CMC', 'It is now forbidden to use specific names for \"flower\", \"tree\" or \"vegetable\", such as daisy, mimosa, magnolia, artichoke, pumpkin, potato... Scientific terms used to describe a flower, plant or tree are banned, as they are no longer useful because of the disappearance of these plants and the risk of misunderstanding. Instead, generic, descriptive expressions such as \"pink flower\", \"large/small tree\", \"green vegetable\" or \"small round vegetable\" will be used.\r\nThese linguistic usages are obviously correlated with an agri-food culture that favours the monoculture of hardy fruit and vegetables offering good yields to feed the greatest number of people.\r\nAny unconventional use of language or culture will be punished. -- The CMC\r\n', 1, NULL, 'Decree on Plants', 'decree'),
(3, 'Coach Potato', 'The disappearance of these plant names, and of the plants themselves, makes me despair. Everyone, whether Mr. or Mrs. Potato, should react in the same way. Let\'s not be couch potatoes, let\'s revolt so that this decree becomes a hot potato in the hands of the CMC! The best way to rebel might be to sow to all winds...', 2, 'against', 'Potato', 'contribution'),
(4, 'Sibylline Dangers', 'Basswood, squash, oak, orchid, apple tree, bean, cherry tree, plane tree... And now \"potato\"! This time, those damned linguists at the Center of Monoculture have even banned  \"potato \" from the latest edition of the CMC Official Dictionary: we will now only dine, frugal and indifferent, on anything but vegetable purees. \r\nAnd, only on special days: a fruit for dessert, possibly with an adjective to describe its size, shape or colour. Tonight, like every other night, I will eat a soup of large vegetables and a round fruit with my eyes glazed over in front of my screen. Austere. Like the bland taste of these in vitro crossbred substitutes: Unbeatable Nutriscore, of course. They started with the trees. Already, during the Rebellion, no one had flinched at seeing cocobolo or pokemoboy stricken from the dictionary, me first [Doc. 3: photos of trees/Wikipedia page or other database]: so few still knew what species they were attached to in their verdant youth since the words to say them were so rarely used. Why bother? Who had ever seen cocolos and pokemboys anyways ? Have you ? I never did. There had indeed been some chatter, when they had refrancised the baobab and araucaria into bobe and aroque. Other plants with foreign names followed: pomel, curcume, dèkon, patchoix cabbage, rutabagas... That was before they simply disappeared from the shelves. Too expensive, too difficult to grow, not energetically efficient enough, compared to their laboratory vegetables and intensive farming. \r\n\r\n\r\nHow far will they go to cut down our nature ? How far will we accept this sterilisation of the world, which has been so extensive as our language ? How far? We must act! So many plant species have already disappeared. Do they now exist only in distant memories? If only some far-sighted people had saved some seeds, seeds from all over the world: then it might be possible to make them germinate again in every latitude! But who could have imagined... And what bank would have been foolish enough to contain such a dream in seeds?\r\n', 2, 'against', 'Ecological and linguistic purification', 'contribution'),
(5, 'mathilde', 'in english... ?', 4, 'neutral', 'writing...', 'contribution'),
(6, 'Essai', 'Try', 4, 'against', 'Essai', 'contribution');

-- --------------------------------------------------------

--
-- Structure de la table `SessionInfos`
--

CREATE TABLE IF NOT EXISTS `SessionInfos` (
  `id` int NOT NULL,
  `title` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `completed` tinyint(1) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `public` tinyint(1) NOT NULL,
  `scenario` int NOT NULL,
  FOREIGN KEY (`scenario`) REFERENCES Scenarios(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `SessionInfos`
--

INSERT INTO `SessionInfos` (`id`, `title`, `author`, `image`, `completed`, `visible`, `createdAt`, `updatedAt`, `public`, `scenario`) VALUES
(67, 'Session publique 14/06 - 14/07', 'superadmin', 'graphe1.png', 0, 0, '2023-06-14 08:47:03', '2023-06-14 08:47:03', 1, 1),
(74, 'Atelier LEEL 15 juin', 'Serge', 'graphe1.png', 1, 1, '2023-06-14 19:50:20', '2023-06-15 14:33:39', 0, 1),
(90, 'ELO_English', 'Serge', 'graphe1.png', 0, 0, '2023-07-04 11:51:40', '2023-07-04 11:51:40', 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `Users`
--

CREATE TABLE IF NOT EXISTS `Users` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `privileges` int NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `Users`
--

INSERT INTO `Users` (`username`, `password`, `privileges`, `createdAt`, `updatedAt`) VALUES
('admin2', '$2b$10$ovBNRHi11yAYPsHXaEZzeOsZuMSzXCRdVGys6GGGsMf7L/neekWXK', 2, '2023-05-04 22:58:02', '2023-05-04 22:58:02'),
('Lucas', '$2b$10$bI0r.OJJfRBHxQtvDuAr7.BW0KgvW7C8..UcV71yG7aPqbMMYa9ci', 1, '2023-05-04 22:57:50', '2023-06-01 00:51:50'),
('mathilde', '$2b$10$AfvrrJ75yb4GjN6OHZsTreTyIJai2K0/zn6rpzIR6coHMr8a4ntVe', 1, '2023-06-03 10:12:21', '2023-06-03 10:12:21'),
('Serge', '$2b$10$ttRrMwFqGh92M6WWkOfmnu3lm64.WLmWsGJFY1cExJ6vUQiyTjI3a', 1, '2023-05-09 15:48:37', '2023-05-09 15:48:37'),
('Simon', '$2b$10$OFJD5TBEj6YXFZK.SGO.le85Lm7FS2Xb.Bx6wg8IT3EaqpkhwTg4e', 1, '2023-05-09 18:06:18', '2023-05-09 18:06:18'),
('solene', '$2b$10$6mMBiuKcrstOAe3wbSkK3uYgfgzDC5Vb5e1zj0FC9PEnZ8wZULzVy', 2, '2023-06-14 06:58:19', '2023-06-14 06:58:19'),
('superadmin', '$2b$10$blNN6WNfvhcYQGLAtTKQVuaBggQmY.U6BovFbR1WtG9v8AdMA2/hC', 0, '2023-05-03 15:05:01', '2023-07-04 11:45:10');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `SequelizeMeta`
--
ALTER TABLE `SequelizeMeta`
  ADD PRIMARY KEY (`name`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Index pour la table `session-67s`
--
ALTER TABLE `session-67s`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `session-74s`
--
ALTER TABLE `session-74s`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `session-90s`
--
ALTER TABLE `session-90s`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `SessionInfos`
--
ALTER TABLE `SessionInfos`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT pour les tables déchargées
--
--
-- AUTO_INCREMENT pour la table `Decrees`
--
ALTER TABLE `Decrees`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `Ends`
--
ALTER TABLE `Ends`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `Examples`
--
ALTER TABLE `Examples`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT pour la table `session-67s`
--
ALTER TABLE `session-67s`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT pour la table `session-74s`
--
ALTER TABLE `session-74s`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT pour la table `session-90s`
--
ALTER TABLE `session-90s`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `SessionInfos`
--
ALTER TABLE `SessionInfos`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
